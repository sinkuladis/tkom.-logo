# Technik Kompilacji

*Autor: Uladzislau Sinkevich (Władysław Sinkiewicz)*

## Specyfikacja problemu

### Interpreter języka Logo


Celem projektu było stworzenie interpretera języka Logo, który jest w stanie wykonać proste operacje arytmietyczne oraz rysować na ekranie graficznym. Żółw, który rysuje na ekranie, jest sterowany programem użytkownika. W ramach projektu był zbudowany interfejs graficzny wraz z wygodnym edytorem tekstu. Interfejs informuje użytkownika o popełnioncyh błędach w kodzie. Oprócz informacji o niezgodności semantyki języka w okienku dialogowym wyświetlane są oczekiwane symbole, co pozwala na szybsze poprawinie błędów. 


### Wymagania funkcjonalne

* Odczyt, zapis plików z programem napisanym w języku Logo.
* Kontrolna poprawności wprowadzonych komend. Wyświetlenie dokładnego miescja błędu składniowego oraz zalecane rozwiązanie problemu.
* Zdefiniowanie własnych funkcji, zmiennych, ~~kolekcji~~.  
* Operacji logiczne, arytmietyczne. Priorytety operacji.
* ~~Funkjce zagniżdżone~~.
* ...


### Wymagania niefunkjonalne 


* Komunikaty o błedach są czytelne.
* Interfejs użytkownika jest intuicyjny i prosty w obsłudze.


## Opis rozwiązania
### Parser i lekser


Lekser i parser interpretera Logo były wygenerowane przy pomocy ANTLR'a. ANTLR jako wejście dostaje plik w formaci *.g4*, który jest podzielony na dwie logiczne częście. Pierwszcza część pliku *.g4* zawiera reguły gramatyki użyte do generowania parsera. Gramatyka, którą odczytuje ANTLR, musi być zgodna z założeniami algorytmu tworzenia parsera LL(\*) - lewostronnym parserem z podglądem dowolnej liczby symboli. Druga część pliku *.g4* zawiera definicji tokenów, które będą rozpoznawane lekserem i podawane parserowi do dalszej analizy wprowadzonego programu.

Plik wejściowy ANTLR'a, który zawiera gramatykę parsera oraz definicji tokenów - [logo.g4](/logo/turtle-graphics/logo.g4).


### Drzewa rozbioru


Drzewo rozbioru jest wynikiem przeprowadzonej analizy składniowej programu. Patrząc na graficzne reprezentację drzewa możemy w przyszłości uniknąć popełnienie błedów podczas projektowania reguł syntaktycznych języka. 

Następny rozdiał ilustruje niektóre przykładowe wyprowadzenia drzew rozbioru programów napisanych w języku Logo. Wszytkie przypadki testowe progmów Logo można znaleź pod tym linkiem - [przykładowe programy Logo](/logo/turtle-graphics/examples).


#### [Przykład 1](/logo/turtle-graphics/examples/example2.txt)

```
cs pu setxy -60 60 pd home rt 45 fd 85 lt 135 fd 120
```

![Drzewo rozbioru](docs/img/image3.png)
<!-- <img src="docs/img/image3.png"  width="465" height="200"> -->

#### [Przykład 2](/logo/turtle-graphics/examples/example4.txt)

```
make "angle 0
	repeat 1000 [fd 3 rt :angle make "angle :angle + 7]
```

![Drzewo rozbioru](docs/img/image.png)
<!-- <img src="docs/img/image.png"  width="465" height="200"> -->


#### [Prykład 3](/logo/turtle-graphics/examples/example4.txt)

```
to star	to walk_the_stars
	repeat 5 [fd 10 rt 144]	fd 20 rt random 360
	end	

	star
	walk_the_stars
	end
```

![Drzewo rozbioru](docs/img/image2.png)
<!-- <img src="docs/img/image2.png"  width="465" height="200"> -->


## Implementeacja interpretera

### Instrukji

Instrukcjami dla żółwia są komendy, w wyniku wykonania których może zmienić się aktualny stan żółwia lub wypisać się komunikat na standardowe wyście. Przykład takich instrukcji: `fd 20`, `rt 30`, `print "Hello!`. Instrukcji dostają nazwę komendy oraz liczbę lub napis, jeśli są wymagane. Wykonanie instrukcji następuje po wywołaniu metody `execute`, podając jako parametr żółwia przywiązanego go sceny.

Opócz liczby lub napisu jako parametr instrukcji może być podane wyrażanie. Wtedy interpreter najpierw oblicza wartość tego wyrażenie i podstawia na jego mejsce wynik.

Interfejs instrukcji lub komunikatów dla żółwia można zobaczyć w pliku [statement.h](/logo/turtle-graphics/src/statement.h). 

### Zmienne

Globalne i lokalne zmienne są ciasno powiązane ze środowiskiem, w którym są one wołane. Interpreter Logo implementuje takie środowskie, w którym możliwe jest definiowanie lokalnych i globalnych zmiennych, ich przesłanianie oraz przetzymywanie kontekstu funkcji użytkownika.

Środowisko, w którym są przetrzymywane zmienne globalne, jest konstruowane jeden raz przy wywołania głównej fukcji interpretera. Nowe środowiska są tworzone podczas wykonywania funkcji lub wejścia w instrukcji, które sterują przepływem programu (np. `for`, `repeat`, `if`). Wszytskie środowskia opócz globalnego posiadją wskazanie na środowskie globalne albo środowko w kontekście którege były skonstruowane. Zapewnia to bezproblemowe wprowadzenie mechanizmu przesłanania w interpreterze.

Interfesj środwiska jest zaimplementowane w pliku [environment.h](/logo/turtle-graphics/src/environment.h).

### Sterowanie przepływem

Za sterowanie odpiawiadają takie instrukcje jak `if`, `repeat`, `for` i td. 

Kroki, które są podejmowane przy wykonaniu instrukcji sterujących, są zdefiniowane w pliku [logovisitor.h](/logo/turtle-graphics/src/logovisitor.h) w funkcjach `visitFor` dla instrucji `for`, `visitRepeat` dla instrukcji `repeat` i td.

### Funkcje

Funkcje użytkowanika są przetrzymywane w globalnym środowski interpretera. Funkcja jest chroniona jako obiekt fukcyjny - obiekt, który trzyma kontekst funkcji (jej definicje).

Wywołanie funkcji powoduje wyszukanie w środowisku globalnym objektu reprezentującego tą funkcję, w przypadku niepowodzenia zgłaszany jest komunikat o nieistnieniu zmiennej o podanej nazwie. Następnie są sprawdzane wartości argumentów wółanej funkcji, po czym są one wiązane z parametrami w loklanie stworzonym środowisku. 

## Interfejs użytkownika

Wygląd interfejsu uzytkownika, przy pierwszym uruchomieniu programu.

<img src="docs/img2/screen_main1.png" height="500"/>


### Porównanie wyników

W tej sekcji jest przedstawione porównanie obrazu z dokumentacji wstępnej z obrazem, który resuje program. W niektórych instrukcja byłe zmieniony arguenty (np. `:x + 1` and `:x + 3`), żeby dopasować skale obrazu programu do obrazów wstępnych.

```
make "x 1 repeat 150 [fd :x rt 89 make "x :x + 1]
```

<img src="docs/img/spiral2.png"><img src="docs/img2/spiral_fin.png" height="300">

<br/>

```
for [i 0 2000] [fd 5 rt (90 * sin :i)]
```
<img src="docs/img/sin.png"><img src="docs/img2/sin_fin.png" height="300">

<br/>

```
repeat 10000 [fd 3 * (-1 + random 2) rt 90 * random 4]
```
<img src="docs/img/rand.png"><img src="docs/img2/random_fin2.png" height="250">

<br/>