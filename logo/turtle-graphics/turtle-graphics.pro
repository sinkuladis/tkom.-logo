#-------------------------------------------------
#
# Project created by QtCreator 2019-03-19T21:40:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = turtle-graphics
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++17 no_keywords

SOURCES += \
    generated/logoBaseListener.cpp \
    generated/logoBaseVisitor.cpp \
    generated/logoLexer.cpp \
    generated/logoListener.cpp \
    generated/logoParser.cpp \
    generated/logoVisitor.cpp \
    src/codeeditor.cpp \
    src/environment.cpp \
    src/logoprocedure.cpp \
    src/logovisitor.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/parser.cpp \
    src/statement.cpp \
    src/throwingerrorlistener.cpp \
    src/turtle.cpp \
    src/statement_error.cpp

HEADERS += \
    generated/logoBaseListener.h \
    generated/logoBaseVisitor.h \
    generated/logoLexer.h \
    generated/logoListener.h \
    generated/logoParser.h \
    generated/logoVisitor.h \
    src/codeeditor.h \
    src/environment.h \
    src/logoprocedure.h \
    src/logovisitor.h \
    src/mainwindow.h \
    src/parser.h \
    src/statement.h \
    src/statement_error.h \
    src/throwingerrorlistener.h \
    src/turtle.h

FORMS += \
    forms/mainwindow.ui

DESTDIR = $$PWD/../build/turtle-graphics

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    generated/logo.tokens \
    generated/logo.tokens \
    generated/logoLexer.tokens \
    generated/logoLexer.tokens

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/release/ -lantlr4-runtime
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/debug/ -lantlr4-runtime
else:unix: LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/ -lantlr4-runtime

INCLUDEPATH += $$PWD/../3rdparty/antlr4-runtime/include \
    $$PWD/../3rdparty
DEPENDPATH += $$PWD/../3rdparty/antlr4-runtime/include \
    $$PWD/../3rdparty
