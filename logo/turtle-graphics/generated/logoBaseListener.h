
// Generated from /home/usinkevi/U/tkom/tkom.-logo/logo/turtle-graphics/logo.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "logoListener.h"


/**
 * This class provides an empty implementation of logoListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  logoBaseListener : public logoListener {
public:

  virtual void enterProg(logoParser::ProgContext * /*ctx*/) override { }
  virtual void exitProg(logoParser::ProgContext * /*ctx*/) override { }

  virtual void enterLine(logoParser::LineContext * /*ctx*/) override { }
  virtual void exitLine(logoParser::LineContext * /*ctx*/) override { }

  virtual void enterCmd(logoParser::CmdContext * /*ctx*/) override { }
  virtual void exitCmd(logoParser::CmdContext * /*ctx*/) override { }

  virtual void enterControl(logoParser::ControlContext * /*ctx*/) override { }
  virtual void exitControl(logoParser::ControlContext * /*ctx*/) override { }

  virtual void enterStatement(logoParser::StatementContext * /*ctx*/) override { }
  virtual void exitStatement(logoParser::StatementContext * /*ctx*/) override { }

  virtual void enterProcedureInvocation(logoParser::ProcedureInvocationContext * /*ctx*/) override { }
  virtual void exitProcedureInvocation(logoParser::ProcedureInvocationContext * /*ctx*/) override { }

  virtual void enterProcedureDeclaration(logoParser::ProcedureDeclarationContext * /*ctx*/) override { }
  virtual void exitProcedureDeclaration(logoParser::ProcedureDeclarationContext * /*ctx*/) override { }

  virtual void enterProcedureBlock(logoParser::ProcedureBlockContext * /*ctx*/) override { }
  virtual void exitProcedureBlock(logoParser::ProcedureBlockContext * /*ctx*/) override { }

  virtual void enterParameterDeclarations(logoParser::ParameterDeclarationsContext * /*ctx*/) override { }
  virtual void exitParameterDeclarations(logoParser::ParameterDeclarationsContext * /*ctx*/) override { }

  virtual void enterFunc(logoParser::FuncContext * /*ctx*/) override { }
  virtual void exitFunc(logoParser::FuncContext * /*ctx*/) override { }

  virtual void enterRepeat(logoParser::RepeatContext * /*ctx*/) override { }
  virtual void exitRepeat(logoParser::RepeatContext * /*ctx*/) override { }

  virtual void enterBlock(logoParser::BlockContext * /*ctx*/) override { }
  virtual void exitBlock(logoParser::BlockContext * /*ctx*/) override { }

  virtual void enterIfe(logoParser::IfeContext * /*ctx*/) override { }
  virtual void exitIfe(logoParser::IfeContext * /*ctx*/) override { }

  virtual void enterComparisonOperator(logoParser::ComparisonOperatorContext * /*ctx*/) override { }
  virtual void exitComparisonOperator(logoParser::ComparisonOperatorContext * /*ctx*/) override { }

  virtual void enterMake(logoParser::MakeContext * /*ctx*/) override { }
  virtual void exitMake(logoParser::MakeContext * /*ctx*/) override { }

  virtual void enterPrint(logoParser::PrintContext * /*ctx*/) override { }
  virtual void exitPrint(logoParser::PrintContext * /*ctx*/) override { }

  virtual void enterName(logoParser::NameContext * /*ctx*/) override { }
  virtual void exitName(logoParser::NameContext * /*ctx*/) override { }

  virtual void enterValue(logoParser::ValueContext * /*ctx*/) override { }
  virtual void exitValue(logoParser::ValueContext * /*ctx*/) override { }

  virtual void enterExpression(logoParser::ExpressionContext * /*ctx*/) override { }
  virtual void exitExpression(logoParser::ExpressionContext * /*ctx*/) override { }

  virtual void enterArithmeticExpression(logoParser::ArithmeticExpressionContext * /*ctx*/) override { }
  virtual void exitArithmeticExpression(logoParser::ArithmeticExpressionContext * /*ctx*/) override { }

  virtual void enterArithmeticNegation(logoParser::ArithmeticNegationContext * /*ctx*/) override { }
  virtual void exitArithmeticNegation(logoParser::ArithmeticNegationContext * /*ctx*/) override { }

  virtual void enterLogicalExpression(logoParser::LogicalExpressionContext * /*ctx*/) override { }
  virtual void exitLogicalExpression(logoParser::LogicalExpressionContext * /*ctx*/) override { }

  virtual void enterComparisonExpression(logoParser::ComparisonExpressionContext * /*ctx*/) override { }
  virtual void exitComparisonExpression(logoParser::ComparisonExpressionContext * /*ctx*/) override { }

  virtual void enterCompOperator(logoParser::CompOperatorContext * /*ctx*/) override { }
  virtual void exitCompOperator(logoParser::CompOperatorContext * /*ctx*/) override { }

  virtual void enterAtom(logoParser::AtomContext * /*ctx*/) override { }
  virtual void exitAtom(logoParser::AtomContext * /*ctx*/) override { }

  virtual void enterDeref(logoParser::DerefContext * /*ctx*/) override { }
  virtual void exitDeref(logoParser::DerefContext * /*ctx*/) override { }

  virtual void enterFd(logoParser::FdContext * /*ctx*/) override { }
  virtual void exitFd(logoParser::FdContext * /*ctx*/) override { }

  virtual void enterBk(logoParser::BkContext * /*ctx*/) override { }
  virtual void exitBk(logoParser::BkContext * /*ctx*/) override { }

  virtual void enterRt(logoParser::RtContext * /*ctx*/) override { }
  virtual void exitRt(logoParser::RtContext * /*ctx*/) override { }

  virtual void enterLt(logoParser::LtContext * /*ctx*/) override { }
  virtual void exitLt(logoParser::LtContext * /*ctx*/) override { }

  virtual void enterCs(logoParser::CsContext * /*ctx*/) override { }
  virtual void exitCs(logoParser::CsContext * /*ctx*/) override { }

  virtual void enterPu(logoParser::PuContext * /*ctx*/) override { }
  virtual void exitPu(logoParser::PuContext * /*ctx*/) override { }

  virtual void enterPd(logoParser::PdContext * /*ctx*/) override { }
  virtual void exitPd(logoParser::PdContext * /*ctx*/) override { }

  virtual void enterHt(logoParser::HtContext * /*ctx*/) override { }
  virtual void exitHt(logoParser::HtContext * /*ctx*/) override { }

  virtual void enterSt(logoParser::StContext * /*ctx*/) override { }
  virtual void exitSt(logoParser::StContext * /*ctx*/) override { }

  virtual void enterHome(logoParser::HomeContext * /*ctx*/) override { }
  virtual void exitHome(logoParser::HomeContext * /*ctx*/) override { }

  virtual void enterStop(logoParser::StopContext * /*ctx*/) override { }
  virtual void exitStop(logoParser::StopContext * /*ctx*/) override { }

  virtual void enterLabel(logoParser::LabelContext * /*ctx*/) override { }
  virtual void exitLabel(logoParser::LabelContext * /*ctx*/) override { }

  virtual void enterSetxy(logoParser::SetxyContext * /*ctx*/) override { }
  virtual void exitSetxy(logoParser::SetxyContext * /*ctx*/) override { }

  virtual void enterRandom(logoParser::RandomContext * /*ctx*/) override { }
  virtual void exitRandom(logoParser::RandomContext * /*ctx*/) override { }

  virtual void enterSin(logoParser::SinContext * /*ctx*/) override { }
  virtual void exitSin(logoParser::SinContext * /*ctx*/) override { }

  virtual void enterCos(logoParser::CosContext * /*ctx*/) override { }
  virtual void exitCos(logoParser::CosContext * /*ctx*/) override { }

  virtual void enterFore(logoParser::ForeContext * /*ctx*/) override { }
  virtual void exitFore(logoParser::ForeContext * /*ctx*/) override { }

  virtual void enterNumber(logoParser::NumberContext * /*ctx*/) override { }
  virtual void exitNumber(logoParser::NumberContext * /*ctx*/) override { }

  virtual void enterComment(logoParser::CommentContext * /*ctx*/) override { }
  virtual void exitComment(logoParser::CommentContext * /*ctx*/) override { }

  virtual void enterStringliteral(logoParser::StringliteralContext * /*ctx*/) override { }
  virtual void exitStringliteral(logoParser::StringliteralContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

