
// Generated from /home/usinkevi/U/tkom/tkom.-logo/logo/turtle-graphics/logo.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "logoVisitor.h"


/**
 * This class provides an empty implementation of logoVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  logoBaseVisitor : public logoVisitor {
public:

  virtual antlrcpp::Any visitProg(logoParser::ProgContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLine(logoParser::LineContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCmd(logoParser::CmdContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitControl(logoParser::ControlContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStatement(logoParser::StatementContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProcedureInvocation(logoParser::ProcedureInvocationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProcedureDeclaration(logoParser::ProcedureDeclarationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitProcedureBlock(logoParser::ProcedureBlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitParameterDeclarations(logoParser::ParameterDeclarationsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFunc(logoParser::FuncContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRepeat(logoParser::RepeatContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBlock(logoParser::BlockContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitIfe(logoParser::IfeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitComparisonOperator(logoParser::ComparisonOperatorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMake(logoParser::MakeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPrint(logoParser::PrintContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitName(logoParser::NameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitValue(logoParser::ValueContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitExpression(logoParser::ExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArithmeticExpression(logoParser::ArithmeticExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitArithmeticNegation(logoParser::ArithmeticNegationContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLogicalExpression(logoParser::LogicalExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitComparisonExpression(logoParser::ComparisonExpressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCompOperator(logoParser::CompOperatorContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAtom(logoParser::AtomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitDeref(logoParser::DerefContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFd(logoParser::FdContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitBk(logoParser::BkContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRt(logoParser::RtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLt(logoParser::LtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCs(logoParser::CsContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPu(logoParser::PuContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPd(logoParser::PdContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitHt(logoParser::HtContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSt(logoParser::StContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitHome(logoParser::HomeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStop(logoParser::StopContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitLabel(logoParser::LabelContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSetxy(logoParser::SetxyContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitRandom(logoParser::RandomContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitSin(logoParser::SinContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitCos(logoParser::CosContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitFore(logoParser::ForeContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitNumber(logoParser::NumberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitComment(logoParser::CommentContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitStringliteral(logoParser::StringliteralContext *ctx) override {
    return visitChildren(ctx);
  }


};

