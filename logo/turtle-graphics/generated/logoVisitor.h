
// Generated from /home/usinkevi/U/tkom/tkom.-logo/logo/turtle-graphics/logo.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "logoParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by logoParser.
 */
class  logoVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by logoParser.
   */
    virtual antlrcpp::Any visitProg(logoParser::ProgContext *context) = 0;

    virtual antlrcpp::Any visitLine(logoParser::LineContext *context) = 0;

    virtual antlrcpp::Any visitCmd(logoParser::CmdContext *context) = 0;

    virtual antlrcpp::Any visitControl(logoParser::ControlContext *context) = 0;

    virtual antlrcpp::Any visitStatement(logoParser::StatementContext *context) = 0;

    virtual antlrcpp::Any visitProcedureInvocation(logoParser::ProcedureInvocationContext *context) = 0;

    virtual antlrcpp::Any visitProcedureDeclaration(logoParser::ProcedureDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitProcedureBlock(logoParser::ProcedureBlockContext *context) = 0;

    virtual antlrcpp::Any visitParameterDeclarations(logoParser::ParameterDeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitFunc(logoParser::FuncContext *context) = 0;

    virtual antlrcpp::Any visitRepeat(logoParser::RepeatContext *context) = 0;

    virtual antlrcpp::Any visitBlock(logoParser::BlockContext *context) = 0;

    virtual antlrcpp::Any visitIfe(logoParser::IfeContext *context) = 0;

    virtual antlrcpp::Any visitComparisonOperator(logoParser::ComparisonOperatorContext *context) = 0;

    virtual antlrcpp::Any visitMake(logoParser::MakeContext *context) = 0;

    virtual antlrcpp::Any visitPrint(logoParser::PrintContext *context) = 0;

    virtual antlrcpp::Any visitName(logoParser::NameContext *context) = 0;

    virtual antlrcpp::Any visitValue(logoParser::ValueContext *context) = 0;

    virtual antlrcpp::Any visitExpression(logoParser::ExpressionContext *context) = 0;

    virtual antlrcpp::Any visitArithmeticExpression(logoParser::ArithmeticExpressionContext *context) = 0;

    virtual antlrcpp::Any visitArithmeticNegation(logoParser::ArithmeticNegationContext *context) = 0;

    virtual antlrcpp::Any visitLogicalExpression(logoParser::LogicalExpressionContext *context) = 0;

    virtual antlrcpp::Any visitComparisonExpression(logoParser::ComparisonExpressionContext *context) = 0;

    virtual antlrcpp::Any visitCompOperator(logoParser::CompOperatorContext *context) = 0;

    virtual antlrcpp::Any visitAtom(logoParser::AtomContext *context) = 0;

    virtual antlrcpp::Any visitDeref(logoParser::DerefContext *context) = 0;

    virtual antlrcpp::Any visitFd(logoParser::FdContext *context) = 0;

    virtual antlrcpp::Any visitBk(logoParser::BkContext *context) = 0;

    virtual antlrcpp::Any visitRt(logoParser::RtContext *context) = 0;

    virtual antlrcpp::Any visitLt(logoParser::LtContext *context) = 0;

    virtual antlrcpp::Any visitCs(logoParser::CsContext *context) = 0;

    virtual antlrcpp::Any visitPu(logoParser::PuContext *context) = 0;

    virtual antlrcpp::Any visitPd(logoParser::PdContext *context) = 0;

    virtual antlrcpp::Any visitHt(logoParser::HtContext *context) = 0;

    virtual antlrcpp::Any visitSt(logoParser::StContext *context) = 0;

    virtual antlrcpp::Any visitHome(logoParser::HomeContext *context) = 0;

    virtual antlrcpp::Any visitStop(logoParser::StopContext *context) = 0;

    virtual antlrcpp::Any visitLabel(logoParser::LabelContext *context) = 0;

    virtual antlrcpp::Any visitSetxy(logoParser::SetxyContext *context) = 0;

    virtual antlrcpp::Any visitRandom(logoParser::RandomContext *context) = 0;

    virtual antlrcpp::Any visitSin(logoParser::SinContext *context) = 0;

    virtual antlrcpp::Any visitCos(logoParser::CosContext *context) = 0;

    virtual antlrcpp::Any visitFore(logoParser::ForeContext *context) = 0;

    virtual antlrcpp::Any visitNumber(logoParser::NumberContext *context) = 0;

    virtual antlrcpp::Any visitComment(logoParser::CommentContext *context) = 0;

    virtual antlrcpp::Any visitStringliteral(logoParser::StringliteralContext *context) = 0;


};

