
// Generated from /home/usinkevi/U/tkom/tkom.-logo/logo/turtle-graphics/logo.g4 by ANTLR 4.7.1


#include "logoListener.h"
#include "logoVisitor.h"

#include "logoParser.h"


using namespace antlrcpp;
using namespace antlr4;

logoParser::logoParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

logoParser::~logoParser() {
  delete _interpreter;
}

std::string logoParser::getGrammarFileName() const {
  return "logo.g4";
}

const std::vector<std::string>& logoParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& logoParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgContext ------------------------------------------------------------------

logoParser::ProgContext::ProgContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> logoParser::ProgContext::EOL() {
  return getTokens(logoParser::EOL);
}

tree::TerminalNode* logoParser::ProgContext::EOL(size_t i) {
  return getToken(logoParser::EOL, i);
}

std::vector<logoParser::LineContext *> logoParser::ProgContext::line() {
  return getRuleContexts<logoParser::LineContext>();
}

logoParser::LineContext* logoParser::ProgContext::line(size_t i) {
  return getRuleContext<logoParser::LineContext>(i);
}


size_t logoParser::ProgContext::getRuleIndex() const {
  return logoParser::RuleProg;
}

void logoParser::ProgContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProg(this);
}

void logoParser::ProgContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProg(this);
}


antlrcpp::Any logoParser::ProgContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitProg(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ProgContext* logoParser::prog() {
  ProgContext *_localctx = _tracker.createInstance<ProgContext>(_ctx, getState());
  enterRule(_localctx, 0, logoParser::RuleProg);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(96); 
    _errHandler->sync(this);
    alt = 1;
    do {
      switch (alt) {
        case 1: {
              setState(93);
              _errHandler->sync(this);

              _la = _input->LA(1);
              if ((((_la & ~ 0x3fULL) == 0) &&
                ((1ULL << _la) & ((1ULL << logoParser::T__0)
                | (1ULL << logoParser::T__3)
                | (1ULL << logoParser::T__6)
                | (1ULL << logoParser::T__7)
                | (1ULL << logoParser::T__8)
                | (1ULL << logoParser::T__9)
                | (1ULL << logoParser::T__10)
                | (1ULL << logoParser::T__11)
                | (1ULL << logoParser::T__12)
                | (1ULL << logoParser::T__13)
                | (1ULL << logoParser::T__14)
                | (1ULL << logoParser::T__15)
                | (1ULL << logoParser::T__16)
                | (1ULL << logoParser::T__17)
                | (1ULL << logoParser::T__18)
                | (1ULL << logoParser::T__19)
                | (1ULL << logoParser::T__20)
                | (1ULL << logoParser::T__21)
                | (1ULL << logoParser::T__22)
                | (1ULL << logoParser::T__23)
                | (1ULL << logoParser::T__24)
                | (1ULL << logoParser::T__25)
                | (1ULL << logoParser::T__26)
                | (1ULL << logoParser::T__27)
                | (1ULL << logoParser::T__29)
                | (1ULL << logoParser::T__30)
                | (1ULL << logoParser::T__34)
                | (1ULL << logoParser::STRING)
                | (1ULL << logoParser::COMMENT))) != 0)) {
                setState(92);
                line();
              }
              setState(95);
              match(logoParser::EOL);
              break;
            }

      default:
        throw NoViableAltException(this);
      }
      setState(98); 
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx);
    } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
    setState(103);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::T__0)
      | (1ULL << logoParser::T__3)
      | (1ULL << logoParser::T__6)
      | (1ULL << logoParser::T__7)
      | (1ULL << logoParser::T__8)
      | (1ULL << logoParser::T__9)
      | (1ULL << logoParser::T__10)
      | (1ULL << logoParser::T__11)
      | (1ULL << logoParser::T__12)
      | (1ULL << logoParser::T__13)
      | (1ULL << logoParser::T__14)
      | (1ULL << logoParser::T__15)
      | (1ULL << logoParser::T__16)
      | (1ULL << logoParser::T__17)
      | (1ULL << logoParser::T__18)
      | (1ULL << logoParser::T__19)
      | (1ULL << logoParser::T__20)
      | (1ULL << logoParser::T__21)
      | (1ULL << logoParser::T__22)
      | (1ULL << logoParser::T__23)
      | (1ULL << logoParser::T__24)
      | (1ULL << logoParser::T__25)
      | (1ULL << logoParser::T__26)
      | (1ULL << logoParser::T__27)
      | (1ULL << logoParser::T__29)
      | (1ULL << logoParser::T__30)
      | (1ULL << logoParser::T__34)
      | (1ULL << logoParser::STRING)
      | (1ULL << logoParser::COMMENT))) != 0)) {
      setState(100);
      line();
      setState(105);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LineContext ------------------------------------------------------------------

logoParser::LineContext::LineContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::CommentContext* logoParser::LineContext::comment() {
  return getRuleContext<logoParser::CommentContext>(0);
}

logoParser::ProcedureDeclarationContext* logoParser::LineContext::procedureDeclaration() {
  return getRuleContext<logoParser::ProcedureDeclarationContext>(0);
}

std::vector<logoParser::CmdContext *> logoParser::LineContext::cmd() {
  return getRuleContexts<logoParser::CmdContext>();
}

logoParser::CmdContext* logoParser::LineContext::cmd(size_t i) {
  return getRuleContext<logoParser::CmdContext>(i);
}


size_t logoParser::LineContext::getRuleIndex() const {
  return logoParser::RuleLine;
}

void logoParser::LineContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLine(this);
}

void logoParser::LineContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLine(this);
}


antlrcpp::Any logoParser::LineContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitLine(this);
  else
    return visitor->visitChildren(this);
}

logoParser::LineContext* logoParser::line() {
  LineContext *_localctx = _tracker.createInstance<LineContext>(_ctx, getState());
  enterRule(_localctx, 2, logoParser::RuleLine);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    size_t alt;
    setState(116);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::COMMENT: {
        enterOuterAlt(_localctx, 1);
        setState(106);
        comment();
        break;
      }

      case logoParser::T__0: {
        enterOuterAlt(_localctx, 2);
        setState(107);
        procedureDeclaration();
        break;
      }

      case logoParser::T__3:
      case logoParser::T__6:
      case logoParser::T__7:
      case logoParser::T__8:
      case logoParser::T__9:
      case logoParser::T__10:
      case logoParser::T__11:
      case logoParser::T__12:
      case logoParser::T__13:
      case logoParser::T__14:
      case logoParser::T__15:
      case logoParser::T__16:
      case logoParser::T__17:
      case logoParser::T__18:
      case logoParser::T__19:
      case logoParser::T__20:
      case logoParser::T__21:
      case logoParser::T__22:
      case logoParser::T__23:
      case logoParser::T__24:
      case logoParser::T__25:
      case logoParser::T__26:
      case logoParser::T__27:
      case logoParser::T__29:
      case logoParser::T__30:
      case logoParser::T__34:
      case logoParser::STRING: {
        enterOuterAlt(_localctx, 3);
        setState(109); 
        _errHandler->sync(this);
        alt = 1;
        do {
          switch (alt) {
            case 1: {
                  setState(108);
                  cmd();
                  break;
                }

          default:
            throw NoViableAltException(this);
          }
          setState(111); 
          _errHandler->sync(this);
          alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx);
        } while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER);
        setState(114);
        _errHandler->sync(this);

        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx)) {
        case 1: {
          setState(113);
          comment();
          break;
        }

        }
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CmdContext ------------------------------------------------------------------

logoParser::CmdContext::CmdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::StatementContext* logoParser::CmdContext::statement() {
  return getRuleContext<logoParser::StatementContext>(0);
}

logoParser::ControlContext* logoParser::CmdContext::control() {
  return getRuleContext<logoParser::ControlContext>(0);
}

logoParser::MakeContext* logoParser::CmdContext::make() {
  return getRuleContext<logoParser::MakeContext>(0);
}

logoParser::ProcedureInvocationContext* logoParser::CmdContext::procedureInvocation() {
  return getRuleContext<logoParser::ProcedureInvocationContext>(0);
}

logoParser::PrintContext* logoParser::CmdContext::print() {
  return getRuleContext<logoParser::PrintContext>(0);
}

logoParser::CommentContext* logoParser::CmdContext::comment() {
  return getRuleContext<logoParser::CommentContext>(0);
}


size_t logoParser::CmdContext::getRuleIndex() const {
  return logoParser::RuleCmd;
}

void logoParser::CmdContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCmd(this);
}

void logoParser::CmdContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCmd(this);
}


antlrcpp::Any logoParser::CmdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitCmd(this);
  else
    return visitor->visitChildren(this);
}

logoParser::CmdContext* logoParser::cmd() {
  CmdContext *_localctx = _tracker.createInstance<CmdContext>(_ctx, getState());
  enterRule(_localctx, 4, logoParser::RuleCmd);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(126);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::T__9:
      case logoParser::T__10:
      case logoParser::T__11:
      case logoParser::T__12:
      case logoParser::T__13:
      case logoParser::T__14:
      case logoParser::T__15:
      case logoParser::T__16:
      case logoParser::T__17:
      case logoParser::T__18:
      case logoParser::T__19:
      case logoParser::T__20:
      case logoParser::T__21:
      case logoParser::T__22:
      case logoParser::T__23:
      case logoParser::T__24:
      case logoParser::T__25:
      case logoParser::T__26:
      case logoParser::T__27:
      case logoParser::T__29:
      case logoParser::T__30: {
        enterOuterAlt(_localctx, 1);
        setState(118);
        statement();
        break;
      }

      case logoParser::T__3:
      case logoParser::T__6:
      case logoParser::T__34: {
        enterOuterAlt(_localctx, 2);
        setState(119);
        control();
        break;
      }

      case logoParser::T__7: {
        enterOuterAlt(_localctx, 3);
        setState(120);
        make();
        break;
      }

      case logoParser::STRING: {
        enterOuterAlt(_localctx, 4);
        setState(121);
        procedureInvocation();
        break;
      }

      case logoParser::T__8: {
        enterOuterAlt(_localctx, 5);
        setState(122);
        print();
        setState(124);
        _errHandler->sync(this);

        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
        case 1: {
          setState(123);
          comment();
          break;
        }

        }
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ControlContext ------------------------------------------------------------------

logoParser::ControlContext::ControlContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::RepeatContext* logoParser::ControlContext::repeat() {
  return getRuleContext<logoParser::RepeatContext>(0);
}

logoParser::IfeContext* logoParser::ControlContext::ife() {
  return getRuleContext<logoParser::IfeContext>(0);
}

logoParser::ForeContext* logoParser::ControlContext::fore() {
  return getRuleContext<logoParser::ForeContext>(0);
}


size_t logoParser::ControlContext::getRuleIndex() const {
  return logoParser::RuleControl;
}

void logoParser::ControlContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterControl(this);
}

void logoParser::ControlContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitControl(this);
}


antlrcpp::Any logoParser::ControlContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitControl(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ControlContext* logoParser::control() {
  ControlContext *_localctx = _tracker.createInstance<ControlContext>(_ctx, getState());
  enterRule(_localctx, 6, logoParser::RuleControl);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(131);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::T__3: {
        enterOuterAlt(_localctx, 1);
        setState(128);
        repeat();
        break;
      }

      case logoParser::T__6: {
        enterOuterAlt(_localctx, 2);
        setState(129);
        ife();
        break;
      }

      case logoParser::T__34: {
        enterOuterAlt(_localctx, 3);
        setState(130);
        fore();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

logoParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::FdContext* logoParser::StatementContext::fd() {
  return getRuleContext<logoParser::FdContext>(0);
}

logoParser::BkContext* logoParser::StatementContext::bk() {
  return getRuleContext<logoParser::BkContext>(0);
}

logoParser::RtContext* logoParser::StatementContext::rt() {
  return getRuleContext<logoParser::RtContext>(0);
}

logoParser::LtContext* logoParser::StatementContext::lt() {
  return getRuleContext<logoParser::LtContext>(0);
}

logoParser::CsContext* logoParser::StatementContext::cs() {
  return getRuleContext<logoParser::CsContext>(0);
}

logoParser::PuContext* logoParser::StatementContext::pu() {
  return getRuleContext<logoParser::PuContext>(0);
}

logoParser::PdContext* logoParser::StatementContext::pd() {
  return getRuleContext<logoParser::PdContext>(0);
}

logoParser::HtContext* logoParser::StatementContext::ht() {
  return getRuleContext<logoParser::HtContext>(0);
}

logoParser::StContext* logoParser::StatementContext::st() {
  return getRuleContext<logoParser::StContext>(0);
}

logoParser::HomeContext* logoParser::StatementContext::home() {
  return getRuleContext<logoParser::HomeContext>(0);
}

logoParser::LabelContext* logoParser::StatementContext::label() {
  return getRuleContext<logoParser::LabelContext>(0);
}

logoParser::SetxyContext* logoParser::StatementContext::setxy() {
  return getRuleContext<logoParser::SetxyContext>(0);
}


size_t logoParser::StatementContext::getRuleIndex() const {
  return logoParser::RuleStatement;
}

void logoParser::StatementContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStatement(this);
}

void logoParser::StatementContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStatement(this);
}


antlrcpp::Any logoParser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

logoParser::StatementContext* logoParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 8, logoParser::RuleStatement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(145);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::T__9:
      case logoParser::T__10: {
        enterOuterAlt(_localctx, 1);
        setState(133);
        fd();
        break;
      }

      case logoParser::T__11:
      case logoParser::T__12: {
        enterOuterAlt(_localctx, 2);
        setState(134);
        bk();
        break;
      }

      case logoParser::T__13:
      case logoParser::T__14: {
        enterOuterAlt(_localctx, 3);
        setState(135);
        rt();
        break;
      }

      case logoParser::T__15:
      case logoParser::T__16: {
        enterOuterAlt(_localctx, 4);
        setState(136);
        lt();
        break;
      }

      case logoParser::T__17:
      case logoParser::T__18: {
        enterOuterAlt(_localctx, 5);
        setState(137);
        cs();
        break;
      }

      case logoParser::T__19:
      case logoParser::T__20: {
        enterOuterAlt(_localctx, 6);
        setState(138);
        pu();
        break;
      }

      case logoParser::T__21:
      case logoParser::T__22: {
        enterOuterAlt(_localctx, 7);
        setState(139);
        pd();
        break;
      }

      case logoParser::T__23:
      case logoParser::T__24: {
        enterOuterAlt(_localctx, 8);
        setState(140);
        ht();
        break;
      }

      case logoParser::T__25:
      case logoParser::T__26: {
        enterOuterAlt(_localctx, 9);
        setState(141);
        st();
        break;
      }

      case logoParser::T__27: {
        enterOuterAlt(_localctx, 10);
        setState(142);
        home();
        break;
      }

      case logoParser::T__29: {
        enterOuterAlt(_localctx, 11);
        setState(143);
        label();
        break;
      }

      case logoParser::T__30: {
        enterOuterAlt(_localctx, 12);
        setState(144);
        setxy();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureInvocationContext ------------------------------------------------------------------

logoParser::ProcedureInvocationContext::ProcedureInvocationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NameContext* logoParser::ProcedureInvocationContext::name() {
  return getRuleContext<logoParser::NameContext>(0);
}

std::vector<logoParser::ExpressionContext *> logoParser::ProcedureInvocationContext::expression() {
  return getRuleContexts<logoParser::ExpressionContext>();
}

logoParser::ExpressionContext* logoParser::ProcedureInvocationContext::expression(size_t i) {
  return getRuleContext<logoParser::ExpressionContext>(i);
}


size_t logoParser::ProcedureInvocationContext::getRuleIndex() const {
  return logoParser::RuleProcedureInvocation;
}

void logoParser::ProcedureInvocationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProcedureInvocation(this);
}

void logoParser::ProcedureInvocationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProcedureInvocation(this);
}


antlrcpp::Any logoParser::ProcedureInvocationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitProcedureInvocation(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ProcedureInvocationContext* logoParser::procedureInvocation() {
  ProcedureInvocationContext *_localctx = _tracker.createInstance<ProcedureInvocationContext>(_ctx, getState());
  enterRule(_localctx, 10, logoParser::RuleProcedureInvocation);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(147);
    name();
    setState(151);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::T__2)
      | (1ULL << logoParser::T__31)
      | (1ULL << logoParser::T__32)
      | (1ULL << logoParser::T__33)
      | (1ULL << logoParser::T__35)
      | (1ULL << logoParser::LPAREN)
      | (1ULL << logoParser::MINUS)
      | (1ULL << logoParser::AND)
      | (1ULL << logoParser::OR)
      | (1ULL << logoParser::TRUE)
      | (1ULL << logoParser::FALSE)
      | (1ULL << logoParser::NUMBER))) != 0)) {
      setState(148);
      expression();
      setState(153);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureDeclarationContext ------------------------------------------------------------------

logoParser::ProcedureDeclarationContext::ProcedureDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NameContext* logoParser::ProcedureDeclarationContext::name() {
  return getRuleContext<logoParser::NameContext>(0);
}

logoParser::ProcedureBlockContext* logoParser::ProcedureDeclarationContext::procedureBlock() {
  return getRuleContext<logoParser::ProcedureBlockContext>(0);
}

std::vector<logoParser::ParameterDeclarationsContext *> logoParser::ProcedureDeclarationContext::parameterDeclarations() {
  return getRuleContexts<logoParser::ParameterDeclarationsContext>();
}

logoParser::ParameterDeclarationsContext* logoParser::ProcedureDeclarationContext::parameterDeclarations(size_t i) {
  return getRuleContext<logoParser::ParameterDeclarationsContext>(i);
}

tree::TerminalNode* logoParser::ProcedureDeclarationContext::EOL() {
  return getToken(logoParser::EOL, 0);
}


size_t logoParser::ProcedureDeclarationContext::getRuleIndex() const {
  return logoParser::RuleProcedureDeclaration;
}

void logoParser::ProcedureDeclarationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProcedureDeclaration(this);
}

void logoParser::ProcedureDeclarationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProcedureDeclaration(this);
}


antlrcpp::Any logoParser::ProcedureDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitProcedureDeclaration(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ProcedureDeclarationContext* logoParser::procedureDeclaration() {
  ProcedureDeclarationContext *_localctx = _tracker.createInstance<ProcedureDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 12, logoParser::RuleProcedureDeclaration);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(154);
    match(logoParser::T__0);
    setState(155);
    name();
    setState(159);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == logoParser::T__2) {
      setState(156);
      parameterDeclarations();
      setState(161);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(163);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
    case 1: {
      setState(162);
      match(logoParser::EOL);
      break;
    }

    }
    setState(165);
    procedureBlock();
    setState(166);
    match(logoParser::T__1);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureBlockContext ------------------------------------------------------------------

logoParser::ProcedureBlockContext::ProcedureBlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> logoParser::ProcedureBlockContext::EOL() {
  return getTokens(logoParser::EOL);
}

tree::TerminalNode* logoParser::ProcedureBlockContext::EOL(size_t i) {
  return getToken(logoParser::EOL, i);
}

std::vector<logoParser::LineContext *> logoParser::ProcedureBlockContext::line() {
  return getRuleContexts<logoParser::LineContext>();
}

logoParser::LineContext* logoParser::ProcedureBlockContext::line(size_t i) {
  return getRuleContext<logoParser::LineContext>(i);
}


size_t logoParser::ProcedureBlockContext::getRuleIndex() const {
  return logoParser::RuleProcedureBlock;
}

void logoParser::ProcedureBlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProcedureBlock(this);
}

void logoParser::ProcedureBlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProcedureBlock(this);
}


antlrcpp::Any logoParser::ProcedureBlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitProcedureBlock(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ProcedureBlockContext* logoParser::procedureBlock() {
  ProcedureBlockContext *_localctx = _tracker.createInstance<ProcedureBlockContext>(_ctx, getState());
  enterRule(_localctx, 14, logoParser::RuleProcedureBlock);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(172); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(169);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << logoParser::T__0)
        | (1ULL << logoParser::T__3)
        | (1ULL << logoParser::T__6)
        | (1ULL << logoParser::T__7)
        | (1ULL << logoParser::T__8)
        | (1ULL << logoParser::T__9)
        | (1ULL << logoParser::T__10)
        | (1ULL << logoParser::T__11)
        | (1ULL << logoParser::T__12)
        | (1ULL << logoParser::T__13)
        | (1ULL << logoParser::T__14)
        | (1ULL << logoParser::T__15)
        | (1ULL << logoParser::T__16)
        | (1ULL << logoParser::T__17)
        | (1ULL << logoParser::T__18)
        | (1ULL << logoParser::T__19)
        | (1ULL << logoParser::T__20)
        | (1ULL << logoParser::T__21)
        | (1ULL << logoParser::T__22)
        | (1ULL << logoParser::T__23)
        | (1ULL << logoParser::T__24)
        | (1ULL << logoParser::T__25)
        | (1ULL << logoParser::T__26)
        | (1ULL << logoParser::T__27)
        | (1ULL << logoParser::T__29)
        | (1ULL << logoParser::T__30)
        | (1ULL << logoParser::T__34)
        | (1ULL << logoParser::STRING)
        | (1ULL << logoParser::COMMENT))) != 0)) {
        setState(168);
        line();
      }
      setState(171);
      match(logoParser::EOL);
      setState(174); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::T__0)
      | (1ULL << logoParser::T__3)
      | (1ULL << logoParser::T__6)
      | (1ULL << logoParser::T__7)
      | (1ULL << logoParser::T__8)
      | (1ULL << logoParser::T__9)
      | (1ULL << logoParser::T__10)
      | (1ULL << logoParser::T__11)
      | (1ULL << logoParser::T__12)
      | (1ULL << logoParser::T__13)
      | (1ULL << logoParser::T__14)
      | (1ULL << logoParser::T__15)
      | (1ULL << logoParser::T__16)
      | (1ULL << logoParser::T__17)
      | (1ULL << logoParser::T__18)
      | (1ULL << logoParser::T__19)
      | (1ULL << logoParser::T__20)
      | (1ULL << logoParser::T__21)
      | (1ULL << logoParser::T__22)
      | (1ULL << logoParser::T__23)
      | (1ULL << logoParser::T__24)
      | (1ULL << logoParser::T__25)
      | (1ULL << logoParser::T__26)
      | (1ULL << logoParser::T__27)
      | (1ULL << logoParser::T__29)
      | (1ULL << logoParser::T__30)
      | (1ULL << logoParser::T__34)
      | (1ULL << logoParser::STRING)
      | (1ULL << logoParser::COMMENT)
      | (1ULL << logoParser::EOL))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterDeclarationsContext ------------------------------------------------------------------

logoParser::ParameterDeclarationsContext::ParameterDeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NameContext* logoParser::ParameterDeclarationsContext::name() {
  return getRuleContext<logoParser::NameContext>(0);
}


size_t logoParser::ParameterDeclarationsContext::getRuleIndex() const {
  return logoParser::RuleParameterDeclarations;
}

void logoParser::ParameterDeclarationsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterParameterDeclarations(this);
}

void logoParser::ParameterDeclarationsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitParameterDeclarations(this);
}


antlrcpp::Any logoParser::ParameterDeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitParameterDeclarations(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ParameterDeclarationsContext* logoParser::parameterDeclarations() {
  ParameterDeclarationsContext *_localctx = _tracker.createInstance<ParameterDeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 16, logoParser::RuleParameterDeclarations);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(176);
    match(logoParser::T__2);
    setState(177);
    name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FuncContext ------------------------------------------------------------------

logoParser::FuncContext::FuncContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::RandomContext* logoParser::FuncContext::random() {
  return getRuleContext<logoParser::RandomContext>(0);
}

logoParser::SinContext* logoParser::FuncContext::sin() {
  return getRuleContext<logoParser::SinContext>(0);
}

logoParser::CosContext* logoParser::FuncContext::cos() {
  return getRuleContext<logoParser::CosContext>(0);
}


size_t logoParser::FuncContext::getRuleIndex() const {
  return logoParser::RuleFunc;
}

void logoParser::FuncContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFunc(this);
}

void logoParser::FuncContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFunc(this);
}


antlrcpp::Any logoParser::FuncContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitFunc(this);
  else
    return visitor->visitChildren(this);
}

logoParser::FuncContext* logoParser::func() {
  FuncContext *_localctx = _tracker.createInstance<FuncContext>(_ctx, getState());
  enterRule(_localctx, 18, logoParser::RuleFunc);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(182);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::T__31: {
        enterOuterAlt(_localctx, 1);
        setState(179);
        random();
        break;
      }

      case logoParser::T__32: {
        enterOuterAlt(_localctx, 2);
        setState(180);
        sin();
        break;
      }

      case logoParser::T__33: {
        enterOuterAlt(_localctx, 3);
        setState(181);
        cos();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RepeatContext ------------------------------------------------------------------

logoParser::RepeatContext::RepeatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NumberContext* logoParser::RepeatContext::number() {
  return getRuleContext<logoParser::NumberContext>(0);
}

logoParser::BlockContext* logoParser::RepeatContext::block() {
  return getRuleContext<logoParser::BlockContext>(0);
}


size_t logoParser::RepeatContext::getRuleIndex() const {
  return logoParser::RuleRepeat;
}

void logoParser::RepeatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRepeat(this);
}

void logoParser::RepeatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRepeat(this);
}


antlrcpp::Any logoParser::RepeatContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitRepeat(this);
  else
    return visitor->visitChildren(this);
}

logoParser::RepeatContext* logoParser::repeat() {
  RepeatContext *_localctx = _tracker.createInstance<RepeatContext>(_ctx, getState());
  enterRule(_localctx, 20, logoParser::RuleRepeat);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(184);
    match(logoParser::T__3);
    setState(185);
    number();
    setState(186);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

logoParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<logoParser::CmdContext *> logoParser::BlockContext::cmd() {
  return getRuleContexts<logoParser::CmdContext>();
}

logoParser::CmdContext* logoParser::BlockContext::cmd(size_t i) {
  return getRuleContext<logoParser::CmdContext>(i);
}


size_t logoParser::BlockContext::getRuleIndex() const {
  return logoParser::RuleBlock;
}

void logoParser::BlockContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBlock(this);
}

void logoParser::BlockContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBlock(this);
}


antlrcpp::Any logoParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

logoParser::BlockContext* logoParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 22, logoParser::RuleBlock);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(188);
    match(logoParser::T__4);
    setState(190); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(189);
      cmd();
      setState(192); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::T__3)
      | (1ULL << logoParser::T__6)
      | (1ULL << logoParser::T__7)
      | (1ULL << logoParser::T__8)
      | (1ULL << logoParser::T__9)
      | (1ULL << logoParser::T__10)
      | (1ULL << logoParser::T__11)
      | (1ULL << logoParser::T__12)
      | (1ULL << logoParser::T__13)
      | (1ULL << logoParser::T__14)
      | (1ULL << logoParser::T__15)
      | (1ULL << logoParser::T__16)
      | (1ULL << logoParser::T__17)
      | (1ULL << logoParser::T__18)
      | (1ULL << logoParser::T__19)
      | (1ULL << logoParser::T__20)
      | (1ULL << logoParser::T__21)
      | (1ULL << logoParser::T__22)
      | (1ULL << logoParser::T__23)
      | (1ULL << logoParser::T__24)
      | (1ULL << logoParser::T__25)
      | (1ULL << logoParser::T__26)
      | (1ULL << logoParser::T__27)
      | (1ULL << logoParser::T__29)
      | (1ULL << logoParser::T__30)
      | (1ULL << logoParser::T__34)
      | (1ULL << logoParser::STRING))) != 0));
    setState(194);
    match(logoParser::T__5);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IfeContext ------------------------------------------------------------------

logoParser::IfeContext::IfeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::BlockContext* logoParser::IfeContext::block() {
  return getRuleContext<logoParser::BlockContext>(0);
}

logoParser::ExpressionContext* logoParser::IfeContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::IfeContext::getRuleIndex() const {
  return logoParser::RuleIfe;
}

void logoParser::IfeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterIfe(this);
}

void logoParser::IfeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitIfe(this);
}


antlrcpp::Any logoParser::IfeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitIfe(this);
  else
    return visitor->visitChildren(this);
}

logoParser::IfeContext* logoParser::ife() {
  IfeContext *_localctx = _tracker.createInstance<IfeContext>(_ctx, getState());
  enterRule(_localctx, 24, logoParser::RuleIfe);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(196);
    match(logoParser::T__6);
    setState(202);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::T__2:
      case logoParser::T__31:
      case logoParser::T__32:
      case logoParser::T__33:
      case logoParser::T__35:
      case logoParser::LPAREN:
      case logoParser::MINUS:
      case logoParser::AND:
      case logoParser::OR:
      case logoParser::TRUE:
      case logoParser::FALSE:
      case logoParser::NUMBER: {
        setState(197);
        expression();
        break;
      }

      case logoParser::T__4: {
        setState(198);
        match(logoParser::T__4);
        setState(199);
        expression();
        setState(200);
        match(logoParser::T__5);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(204);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ComparisonOperatorContext ------------------------------------------------------------------

logoParser::ComparisonOperatorContext::ComparisonOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::ComparisonOperatorContext::EQ() {
  return getToken(logoParser::EQ, 0);
}

tree::TerminalNode* logoParser::ComparisonOperatorContext::GT() {
  return getToken(logoParser::GT, 0);
}

tree::TerminalNode* logoParser::ComparisonOperatorContext::LT() {
  return getToken(logoParser::LT, 0);
}

tree::TerminalNode* logoParser::ComparisonOperatorContext::GET() {
  return getToken(logoParser::GET, 0);
}

tree::TerminalNode* logoParser::ComparisonOperatorContext::LET() {
  return getToken(logoParser::LET, 0);
}


size_t logoParser::ComparisonOperatorContext::getRuleIndex() const {
  return logoParser::RuleComparisonOperator;
}

void logoParser::ComparisonOperatorContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterComparisonOperator(this);
}

void logoParser::ComparisonOperatorContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitComparisonOperator(this);
}


antlrcpp::Any logoParser::ComparisonOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitComparisonOperator(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ComparisonOperatorContext* logoParser::comparisonOperator() {
  ComparisonOperatorContext *_localctx = _tracker.createInstance<ComparisonOperatorContext>(_ctx, getState());
  enterRule(_localctx, 26, logoParser::RuleComparisonOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(206);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::GT)
      | (1ULL << logoParser::GET)
      | (1ULL << logoParser::LT)
      | (1ULL << logoParser::LET)
      | (1ULL << logoParser::EQ))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MakeContext ------------------------------------------------------------------

logoParser::MakeContext::MakeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::StringliteralContext* logoParser::MakeContext::stringliteral() {
  return getRuleContext<logoParser::StringliteralContext>(0);
}

logoParser::ValueContext* logoParser::MakeContext::value() {
  return getRuleContext<logoParser::ValueContext>(0);
}


size_t logoParser::MakeContext::getRuleIndex() const {
  return logoParser::RuleMake;
}

void logoParser::MakeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterMake(this);
}

void logoParser::MakeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitMake(this);
}


antlrcpp::Any logoParser::MakeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitMake(this);
  else
    return visitor->visitChildren(this);
}

logoParser::MakeContext* logoParser::make() {
  MakeContext *_localctx = _tracker.createInstance<MakeContext>(_ctx, getState());
  enterRule(_localctx, 28, logoParser::RuleMake);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(208);
    match(logoParser::T__7);
    setState(209);
    stringliteral();
    setState(210);
    value();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PrintContext ------------------------------------------------------------------

logoParser::PrintContext::PrintContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ValueContext* logoParser::PrintContext::value() {
  return getRuleContext<logoParser::ValueContext>(0);
}


size_t logoParser::PrintContext::getRuleIndex() const {
  return logoParser::RulePrint;
}

void logoParser::PrintContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrint(this);
}

void logoParser::PrintContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrint(this);
}


antlrcpp::Any logoParser::PrintContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitPrint(this);
  else
    return visitor->visitChildren(this);
}

logoParser::PrintContext* logoParser::print() {
  PrintContext *_localctx = _tracker.createInstance<PrintContext>(_ctx, getState());
  enterRule(_localctx, 30, logoParser::RulePrint);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(212);
    match(logoParser::T__8);
    setState(213);
    value();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NameContext ------------------------------------------------------------------

logoParser::NameContext::NameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::NameContext::STRING() {
  return getToken(logoParser::STRING, 0);
}


size_t logoParser::NameContext::getRuleIndex() const {
  return logoParser::RuleName;
}

void logoParser::NameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterName(this);
}

void logoParser::NameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitName(this);
}


antlrcpp::Any logoParser::NameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitName(this);
  else
    return visitor->visitChildren(this);
}

logoParser::NameContext* logoParser::name() {
  NameContext *_localctx = _tracker.createInstance<NameContext>(_ctx, getState());
  enterRule(_localctx, 32, logoParser::RuleName);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(215);
    match(logoParser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ValueContext ------------------------------------------------------------------

logoParser::ValueContext::ValueContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::ValueContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}

logoParser::DerefContext* logoParser::ValueContext::deref() {
  return getRuleContext<logoParser::DerefContext>(0);
}


size_t logoParser::ValueContext::getRuleIndex() const {
  return logoParser::RuleValue;
}

void logoParser::ValueContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterValue(this);
}

void logoParser::ValueContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitValue(this);
}


antlrcpp::Any logoParser::ValueContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitValue(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ValueContext* logoParser::value() {
  ValueContext *_localctx = _tracker.createInstance<ValueContext>(_ctx, getState());
  enterRule(_localctx, 34, logoParser::RuleValue);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(219);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 18, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(217);
      expression();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(218);
      deref();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

logoParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::LogicalExpressionContext* logoParser::ExpressionContext::logicalExpression() {
  return getRuleContext<logoParser::LogicalExpressionContext>(0);
}

logoParser::ArithmeticExpressionContext* logoParser::ExpressionContext::arithmeticExpression() {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(0);
}


size_t logoParser::ExpressionContext::getRuleIndex() const {
  return logoParser::RuleExpression;
}

void logoParser::ExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpression(this);
}

void logoParser::ExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpression(this);
}


antlrcpp::Any logoParser::ExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitExpression(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ExpressionContext* logoParser::expression() {
  ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, getState());
  enterRule(_localctx, 36, logoParser::RuleExpression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(223);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 19, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(221);
      logicalExpression();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(222);
      arithmeticExpression(0);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArithmeticExpressionContext ------------------------------------------------------------------

logoParser::ArithmeticExpressionContext::ArithmeticExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ArithmeticNegationContext* logoParser::ArithmeticExpressionContext::arithmeticNegation() {
  return getRuleContext<logoParser::ArithmeticNegationContext>(0);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::LPAREN() {
  return getToken(logoParser::LPAREN, 0);
}

std::vector<logoParser::ArithmeticExpressionContext *> logoParser::ArithmeticExpressionContext::arithmeticExpression() {
  return getRuleContexts<logoParser::ArithmeticExpressionContext>();
}

logoParser::ArithmeticExpressionContext* logoParser::ArithmeticExpressionContext::arithmeticExpression(size_t i) {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(i);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::RPAREN() {
  return getToken(logoParser::RPAREN, 0);
}

logoParser::AtomContext* logoParser::ArithmeticExpressionContext::atom() {
  return getRuleContext<logoParser::AtomContext>(0);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::TIMES() {
  return getToken(logoParser::TIMES, 0);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::DIV() {
  return getToken(logoParser::DIV, 0);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::PLUS() {
  return getToken(logoParser::PLUS, 0);
}

tree::TerminalNode* logoParser::ArithmeticExpressionContext::MINUS() {
  return getToken(logoParser::MINUS, 0);
}


size_t logoParser::ArithmeticExpressionContext::getRuleIndex() const {
  return logoParser::RuleArithmeticExpression;
}

void logoParser::ArithmeticExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArithmeticExpression(this);
}

void logoParser::ArithmeticExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArithmeticExpression(this);
}


antlrcpp::Any logoParser::ArithmeticExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitArithmeticExpression(this);
  else
    return visitor->visitChildren(this);
}


logoParser::ArithmeticExpressionContext* logoParser::arithmeticExpression() {
   return arithmeticExpression(0);
}

logoParser::ArithmeticExpressionContext* logoParser::arithmeticExpression(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  logoParser::ArithmeticExpressionContext *_localctx = _tracker.createInstance<ArithmeticExpressionContext>(_ctx, parentState);
  logoParser::ArithmeticExpressionContext *previousContext = _localctx;
  size_t startState = 38;
  enterRecursionRule(_localctx, 38, logoParser::RuleArithmeticExpression, precedence);

    

  auto onExit = finally([=] {
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(232);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::MINUS: {
        setState(226);
        arithmeticNegation();
        break;
      }

      case logoParser::LPAREN: {
        setState(227);
        match(logoParser::LPAREN);
        setState(228);
        arithmeticExpression(0);
        setState(229);
        match(logoParser::RPAREN);
        break;
      }

      case logoParser::T__2:
      case logoParser::T__31:
      case logoParser::T__32:
      case logoParser::T__33:
      case logoParser::T__35:
      case logoParser::NUMBER: {
        setState(231);
        atom();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    _ctx->stop = _input->LT(-1);
    setState(248);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(246);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 21, _ctx)) {
        case 1: {
          _localctx = _tracker.createInstance<ArithmeticExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleArithmeticExpression);
          setState(234);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(235);
          match(logoParser::TIMES);
          setState(236);
          arithmeticExpression(8);
          break;
        }

        case 2: {
          _localctx = _tracker.createInstance<ArithmeticExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleArithmeticExpression);
          setState(237);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(238);
          match(logoParser::DIV);
          setState(239);
          arithmeticExpression(7);
          break;
        }

        case 3: {
          _localctx = _tracker.createInstance<ArithmeticExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleArithmeticExpression);
          setState(240);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(241);
          match(logoParser::PLUS);
          setState(242);
          arithmeticExpression(6);
          break;
        }

        case 4: {
          _localctx = _tracker.createInstance<ArithmeticExpressionContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleArithmeticExpression);
          setState(243);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(244);
          match(logoParser::MINUS);
          setState(245);
          arithmeticExpression(5);
          break;
        }

        } 
      }
      setState(250);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 22, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- ArithmeticNegationContext ------------------------------------------------------------------

logoParser::ArithmeticNegationContext::ArithmeticNegationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::ArithmeticNegationContext::MINUS() {
  return getToken(logoParser::MINUS, 0);
}

logoParser::ArithmeticExpressionContext* logoParser::ArithmeticNegationContext::arithmeticExpression() {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(0);
}


size_t logoParser::ArithmeticNegationContext::getRuleIndex() const {
  return logoParser::RuleArithmeticNegation;
}

void logoParser::ArithmeticNegationContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArithmeticNegation(this);
}

void logoParser::ArithmeticNegationContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArithmeticNegation(this);
}


antlrcpp::Any logoParser::ArithmeticNegationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitArithmeticNegation(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ArithmeticNegationContext* logoParser::arithmeticNegation() {
  ArithmeticNegationContext *_localctx = _tracker.createInstance<ArithmeticNegationContext>(_ctx, getState());
  enterRule(_localctx, 40, logoParser::RuleArithmeticNegation);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(251);
    match(logoParser::MINUS);
    setState(252);
    arithmeticExpression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LogicalExpressionContext ------------------------------------------------------------------

logoParser::LogicalExpressionContext::LogicalExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::LogicalExpressionContext::AND() {
  return getToken(logoParser::AND, 0);
}

std::vector<logoParser::LogicalExpressionContext *> logoParser::LogicalExpressionContext::logicalExpression() {
  return getRuleContexts<logoParser::LogicalExpressionContext>();
}

logoParser::LogicalExpressionContext* logoParser::LogicalExpressionContext::logicalExpression(size_t i) {
  return getRuleContext<logoParser::LogicalExpressionContext>(i);
}

tree::TerminalNode* logoParser::LogicalExpressionContext::OR() {
  return getToken(logoParser::OR, 0);
}

logoParser::ComparisonExpressionContext* logoParser::LogicalExpressionContext::comparisonExpression() {
  return getRuleContext<logoParser::ComparisonExpressionContext>(0);
}

tree::TerminalNode* logoParser::LogicalExpressionContext::LPAREN() {
  return getToken(logoParser::LPAREN, 0);
}

tree::TerminalNode* logoParser::LogicalExpressionContext::RPAREN() {
  return getToken(logoParser::RPAREN, 0);
}

tree::TerminalNode* logoParser::LogicalExpressionContext::TRUE() {
  return getToken(logoParser::TRUE, 0);
}

tree::TerminalNode* logoParser::LogicalExpressionContext::FALSE() {
  return getToken(logoParser::FALSE, 0);
}


size_t logoParser::LogicalExpressionContext::getRuleIndex() const {
  return logoParser::RuleLogicalExpression;
}

void logoParser::LogicalExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLogicalExpression(this);
}

void logoParser::LogicalExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLogicalExpression(this);
}


antlrcpp::Any logoParser::LogicalExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitLogicalExpression(this);
  else
    return visitor->visitChildren(this);
}

logoParser::LogicalExpressionContext* logoParser::logicalExpression() {
  LogicalExpressionContext *_localctx = _tracker.createInstance<LogicalExpressionContext>(_ctx, getState());
  enterRule(_localctx, 42, logoParser::RuleLogicalExpression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(269);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 23, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(254);
      match(logoParser::AND);
      setState(255);
      logicalExpression();
      setState(256);
      logicalExpression();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(258);
      match(logoParser::OR);
      setState(259);
      logicalExpression();
      setState(260);
      logicalExpression();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(262);
      comparisonExpression();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(263);
      match(logoParser::LPAREN);
      setState(264);
      logicalExpression();
      setState(265);
      match(logoParser::RPAREN);
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(267);
      match(logoParser::TRUE);
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(268);
      match(logoParser::FALSE);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ComparisonExpressionContext ------------------------------------------------------------------

logoParser::ComparisonExpressionContext::ComparisonExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<logoParser::ArithmeticExpressionContext *> logoParser::ComparisonExpressionContext::arithmeticExpression() {
  return getRuleContexts<logoParser::ArithmeticExpressionContext>();
}

logoParser::ArithmeticExpressionContext* logoParser::ComparisonExpressionContext::arithmeticExpression(size_t i) {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(i);
}

logoParser::CompOperatorContext* logoParser::ComparisonExpressionContext::compOperator() {
  return getRuleContext<logoParser::CompOperatorContext>(0);
}


size_t logoParser::ComparisonExpressionContext::getRuleIndex() const {
  return logoParser::RuleComparisonExpression;
}

void logoParser::ComparisonExpressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterComparisonExpression(this);
}

void logoParser::ComparisonExpressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitComparisonExpression(this);
}


antlrcpp::Any logoParser::ComparisonExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitComparisonExpression(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ComparisonExpressionContext* logoParser::comparisonExpression() {
  ComparisonExpressionContext *_localctx = _tracker.createInstance<ComparisonExpressionContext>(_ctx, getState());
  enterRule(_localctx, 44, logoParser::RuleComparisonExpression);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(271);
    arithmeticExpression(0);
    setState(272);
    compOperator();
    setState(273);
    arithmeticExpression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompOperatorContext ------------------------------------------------------------------

logoParser::CompOperatorContext::CompOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::CompOperatorContext::GT() {
  return getToken(logoParser::GT, 0);
}

tree::TerminalNode* logoParser::CompOperatorContext::GET() {
  return getToken(logoParser::GET, 0);
}

tree::TerminalNode* logoParser::CompOperatorContext::LT() {
  return getToken(logoParser::LT, 0);
}

tree::TerminalNode* logoParser::CompOperatorContext::LET() {
  return getToken(logoParser::LET, 0);
}

tree::TerminalNode* logoParser::CompOperatorContext::EQ() {
  return getToken(logoParser::EQ, 0);
}


size_t logoParser::CompOperatorContext::getRuleIndex() const {
  return logoParser::RuleCompOperator;
}

void logoParser::CompOperatorContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCompOperator(this);
}

void logoParser::CompOperatorContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCompOperator(this);
}


antlrcpp::Any logoParser::CompOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitCompOperator(this);
  else
    return visitor->visitChildren(this);
}

logoParser::CompOperatorContext* logoParser::compOperator() {
  CompOperatorContext *_localctx = _tracker.createInstance<CompOperatorContext>(_ctx, getState());
  enterRule(_localctx, 46, logoParser::RuleCompOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(275);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::GT)
      | (1ULL << logoParser::GET)
      | (1ULL << logoParser::LT)
      | (1ULL << logoParser::LET)
      | (1ULL << logoParser::EQ))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AtomContext ------------------------------------------------------------------

logoParser::AtomContext::AtomContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NumberContext* logoParser::AtomContext::number() {
  return getRuleContext<logoParser::NumberContext>(0);
}

logoParser::DerefContext* logoParser::AtomContext::deref() {
  return getRuleContext<logoParser::DerefContext>(0);
}

logoParser::FuncContext* logoParser::AtomContext::func() {
  return getRuleContext<logoParser::FuncContext>(0);
}

logoParser::StringliteralContext* logoParser::AtomContext::stringliteral() {
  return getRuleContext<logoParser::StringliteralContext>(0);
}


size_t logoParser::AtomContext::getRuleIndex() const {
  return logoParser::RuleAtom;
}

void logoParser::AtomContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAtom(this);
}

void logoParser::AtomContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAtom(this);
}


antlrcpp::Any logoParser::AtomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitAtom(this);
  else
    return visitor->visitChildren(this);
}

logoParser::AtomContext* logoParser::atom() {
  AtomContext *_localctx = _tracker.createInstance<AtomContext>(_ctx, getState());
  enterRule(_localctx, 48, logoParser::RuleAtom);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(281);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case logoParser::NUMBER: {
        enterOuterAlt(_localctx, 1);
        setState(277);
        number();
        break;
      }

      case logoParser::T__2: {
        enterOuterAlt(_localctx, 2);
        setState(278);
        deref();
        break;
      }

      case logoParser::T__31:
      case logoParser::T__32:
      case logoParser::T__33: {
        enterOuterAlt(_localctx, 3);
        setState(279);
        func();
        break;
      }

      case logoParser::T__35: {
        enterOuterAlt(_localctx, 4);
        setState(280);
        stringliteral();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DerefContext ------------------------------------------------------------------

logoParser::DerefContext::DerefContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NameContext* logoParser::DerefContext::name() {
  return getRuleContext<logoParser::NameContext>(0);
}


size_t logoParser::DerefContext::getRuleIndex() const {
  return logoParser::RuleDeref;
}

void logoParser::DerefContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterDeref(this);
}

void logoParser::DerefContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitDeref(this);
}


antlrcpp::Any logoParser::DerefContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitDeref(this);
  else
    return visitor->visitChildren(this);
}

logoParser::DerefContext* logoParser::deref() {
  DerefContext *_localctx = _tracker.createInstance<DerefContext>(_ctx, getState());
  enterRule(_localctx, 50, logoParser::RuleDeref);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(283);
    match(logoParser::T__2);
    setState(284);
    name();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FdContext ------------------------------------------------------------------

logoParser::FdContext::FdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::FdContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::FdContext::getRuleIndex() const {
  return logoParser::RuleFd;
}

void logoParser::FdContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFd(this);
}

void logoParser::FdContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFd(this);
}


antlrcpp::Any logoParser::FdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitFd(this);
  else
    return visitor->visitChildren(this);
}

logoParser::FdContext* logoParser::fd() {
  FdContext *_localctx = _tracker.createInstance<FdContext>(_ctx, getState());
  enterRule(_localctx, 52, logoParser::RuleFd);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(286);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__9

    || _la == logoParser::T__10)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(287);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BkContext ------------------------------------------------------------------

logoParser::BkContext::BkContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::BkContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::BkContext::getRuleIndex() const {
  return logoParser::RuleBk;
}

void logoParser::BkContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterBk(this);
}

void logoParser::BkContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitBk(this);
}


antlrcpp::Any logoParser::BkContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitBk(this);
  else
    return visitor->visitChildren(this);
}

logoParser::BkContext* logoParser::bk() {
  BkContext *_localctx = _tracker.createInstance<BkContext>(_ctx, getState());
  enterRule(_localctx, 54, logoParser::RuleBk);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(289);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__11

    || _la == logoParser::T__12)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(290);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RtContext ------------------------------------------------------------------

logoParser::RtContext::RtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::RtContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::RtContext::getRuleIndex() const {
  return logoParser::RuleRt;
}

void logoParser::RtContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRt(this);
}

void logoParser::RtContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRt(this);
}


antlrcpp::Any logoParser::RtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitRt(this);
  else
    return visitor->visitChildren(this);
}

logoParser::RtContext* logoParser::rt() {
  RtContext *_localctx = _tracker.createInstance<RtContext>(_ctx, getState());
  enterRule(_localctx, 56, logoParser::RuleRt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(292);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__13

    || _la == logoParser::T__14)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(293);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LtContext ------------------------------------------------------------------

logoParser::LtContext::LtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::LtContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::LtContext::getRuleIndex() const {
  return logoParser::RuleLt;
}

void logoParser::LtContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLt(this);
}

void logoParser::LtContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLt(this);
}


antlrcpp::Any logoParser::LtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitLt(this);
  else
    return visitor->visitChildren(this);
}

logoParser::LtContext* logoParser::lt() {
  LtContext *_localctx = _tracker.createInstance<LtContext>(_ctx, getState());
  enterRule(_localctx, 58, logoParser::RuleLt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(295);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__15

    || _la == logoParser::T__16)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(296);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CsContext ------------------------------------------------------------------

logoParser::CsContext::CsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::CsContext::getRuleIndex() const {
  return logoParser::RuleCs;
}

void logoParser::CsContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCs(this);
}

void logoParser::CsContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCs(this);
}


antlrcpp::Any logoParser::CsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitCs(this);
  else
    return visitor->visitChildren(this);
}

logoParser::CsContext* logoParser::cs() {
  CsContext *_localctx = _tracker.createInstance<CsContext>(_ctx, getState());
  enterRule(_localctx, 60, logoParser::RuleCs);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(298);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__17

    || _la == logoParser::T__18)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PuContext ------------------------------------------------------------------

logoParser::PuContext::PuContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::PuContext::getRuleIndex() const {
  return logoParser::RulePu;
}

void logoParser::PuContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPu(this);
}

void logoParser::PuContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPu(this);
}


antlrcpp::Any logoParser::PuContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitPu(this);
  else
    return visitor->visitChildren(this);
}

logoParser::PuContext* logoParser::pu() {
  PuContext *_localctx = _tracker.createInstance<PuContext>(_ctx, getState());
  enterRule(_localctx, 62, logoParser::RulePu);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(300);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__19

    || _la == logoParser::T__20)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PdContext ------------------------------------------------------------------

logoParser::PdContext::PdContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::PdContext::getRuleIndex() const {
  return logoParser::RulePd;
}

void logoParser::PdContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPd(this);
}

void logoParser::PdContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPd(this);
}


antlrcpp::Any logoParser::PdContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitPd(this);
  else
    return visitor->visitChildren(this);
}

logoParser::PdContext* logoParser::pd() {
  PdContext *_localctx = _tracker.createInstance<PdContext>(_ctx, getState());
  enterRule(_localctx, 64, logoParser::RulePd);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(302);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__21

    || _la == logoParser::T__22)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HtContext ------------------------------------------------------------------

logoParser::HtContext::HtContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::HtContext::getRuleIndex() const {
  return logoParser::RuleHt;
}

void logoParser::HtContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterHt(this);
}

void logoParser::HtContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitHt(this);
}


antlrcpp::Any logoParser::HtContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitHt(this);
  else
    return visitor->visitChildren(this);
}

logoParser::HtContext* logoParser::ht() {
  HtContext *_localctx = _tracker.createInstance<HtContext>(_ctx, getState());
  enterRule(_localctx, 66, logoParser::RuleHt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(304);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__23

    || _la == logoParser::T__24)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StContext ------------------------------------------------------------------

logoParser::StContext::StContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::StContext::getRuleIndex() const {
  return logoParser::RuleSt;
}

void logoParser::StContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSt(this);
}

void logoParser::StContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSt(this);
}


antlrcpp::Any logoParser::StContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitSt(this);
  else
    return visitor->visitChildren(this);
}

logoParser::StContext* logoParser::st() {
  StContext *_localctx = _tracker.createInstance<StContext>(_ctx, getState());
  enterRule(_localctx, 68, logoParser::RuleSt);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(306);
    _la = _input->LA(1);
    if (!(_la == logoParser::T__25

    || _la == logoParser::T__26)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- HomeContext ------------------------------------------------------------------

logoParser::HomeContext::HomeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::HomeContext::getRuleIndex() const {
  return logoParser::RuleHome;
}

void logoParser::HomeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterHome(this);
}

void logoParser::HomeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitHome(this);
}


antlrcpp::Any logoParser::HomeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitHome(this);
  else
    return visitor->visitChildren(this);
}

logoParser::HomeContext* logoParser::home() {
  HomeContext *_localctx = _tracker.createInstance<HomeContext>(_ctx, getState());
  enterRule(_localctx, 70, logoParser::RuleHome);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(308);
    match(logoParser::T__27);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StopContext ------------------------------------------------------------------

logoParser::StopContext::StopContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::StopContext::getRuleIndex() const {
  return logoParser::RuleStop;
}

void logoParser::StopContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStop(this);
}

void logoParser::StopContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStop(this);
}


antlrcpp::Any logoParser::StopContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitStop(this);
  else
    return visitor->visitChildren(this);
}

logoParser::StopContext* logoParser::stop() {
  StopContext *_localctx = _tracker.createInstance<StopContext>(_ctx, getState());
  enterRule(_localctx, 72, logoParser::RuleStop);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(310);
    match(logoParser::T__28);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LabelContext ------------------------------------------------------------------

logoParser::LabelContext::LabelContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t logoParser::LabelContext::getRuleIndex() const {
  return logoParser::RuleLabel;
}

void logoParser::LabelContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterLabel(this);
}

void logoParser::LabelContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitLabel(this);
}


antlrcpp::Any logoParser::LabelContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitLabel(this);
  else
    return visitor->visitChildren(this);
}

logoParser::LabelContext* logoParser::label() {
  LabelContext *_localctx = _tracker.createInstance<LabelContext>(_ctx, getState());
  enterRule(_localctx, 74, logoParser::RuleLabel);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(312);
    match(logoParser::T__29);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SetxyContext ------------------------------------------------------------------

logoParser::SetxyContext::SetxyContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<logoParser::ExpressionContext *> logoParser::SetxyContext::expression() {
  return getRuleContexts<logoParser::ExpressionContext>();
}

logoParser::ExpressionContext* logoParser::SetxyContext::expression(size_t i) {
  return getRuleContext<logoParser::ExpressionContext>(i);
}


size_t logoParser::SetxyContext::getRuleIndex() const {
  return logoParser::RuleSetxy;
}

void logoParser::SetxyContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSetxy(this);
}

void logoParser::SetxyContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSetxy(this);
}


antlrcpp::Any logoParser::SetxyContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitSetxy(this);
  else
    return visitor->visitChildren(this);
}

logoParser::SetxyContext* logoParser::setxy() {
  SetxyContext *_localctx = _tracker.createInstance<SetxyContext>(_ctx, getState());
  enterRule(_localctx, 76, logoParser::RuleSetxy);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(314);
    match(logoParser::T__30);
    setState(315);
    expression();
    setState(316);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RandomContext ------------------------------------------------------------------

logoParser::RandomContext::RandomContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ExpressionContext* logoParser::RandomContext::expression() {
  return getRuleContext<logoParser::ExpressionContext>(0);
}


size_t logoParser::RandomContext::getRuleIndex() const {
  return logoParser::RuleRandom;
}

void logoParser::RandomContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterRandom(this);
}

void logoParser::RandomContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitRandom(this);
}


antlrcpp::Any logoParser::RandomContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitRandom(this);
  else
    return visitor->visitChildren(this);
}

logoParser::RandomContext* logoParser::random() {
  RandomContext *_localctx = _tracker.createInstance<RandomContext>(_ctx, getState());
  enterRule(_localctx, 78, logoParser::RuleRandom);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(318);
    match(logoParser::T__31);
    setState(319);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SinContext ------------------------------------------------------------------

logoParser::SinContext::SinContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ArithmeticExpressionContext* logoParser::SinContext::arithmeticExpression() {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(0);
}


size_t logoParser::SinContext::getRuleIndex() const {
  return logoParser::RuleSin;
}

void logoParser::SinContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterSin(this);
}

void logoParser::SinContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitSin(this);
}


antlrcpp::Any logoParser::SinContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitSin(this);
  else
    return visitor->visitChildren(this);
}

logoParser::SinContext* logoParser::sin() {
  SinContext *_localctx = _tracker.createInstance<SinContext>(_ctx, getState());
  enterRule(_localctx, 80, logoParser::RuleSin);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(321);
    match(logoParser::T__32);
    setState(322);
    arithmeticExpression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CosContext ------------------------------------------------------------------

logoParser::CosContext::CosContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::ArithmeticExpressionContext* logoParser::CosContext::arithmeticExpression() {
  return getRuleContext<logoParser::ArithmeticExpressionContext>(0);
}


size_t logoParser::CosContext::getRuleIndex() const {
  return logoParser::RuleCos;
}

void logoParser::CosContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterCos(this);
}

void logoParser::CosContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitCos(this);
}


antlrcpp::Any logoParser::CosContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitCos(this);
  else
    return visitor->visitChildren(this);
}

logoParser::CosContext* logoParser::cos() {
  CosContext *_localctx = _tracker.createInstance<CosContext>(_ctx, getState());
  enterRule(_localctx, 82, logoParser::RuleCos);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(324);
    match(logoParser::T__33);
    setState(325);
    arithmeticExpression(0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ForeContext ------------------------------------------------------------------

logoParser::ForeContext::ForeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

logoParser::NameContext* logoParser::ForeContext::name() {
  return getRuleContext<logoParser::NameContext>(0);
}

std::vector<logoParser::ExpressionContext *> logoParser::ForeContext::expression() {
  return getRuleContexts<logoParser::ExpressionContext>();
}

logoParser::ExpressionContext* logoParser::ForeContext::expression(size_t i) {
  return getRuleContext<logoParser::ExpressionContext>(i);
}

logoParser::BlockContext* logoParser::ForeContext::block() {
  return getRuleContext<logoParser::BlockContext>(0);
}


size_t logoParser::ForeContext::getRuleIndex() const {
  return logoParser::RuleFore;
}

void logoParser::ForeContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterFore(this);
}

void logoParser::ForeContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitFore(this);
}


antlrcpp::Any logoParser::ForeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitFore(this);
  else
    return visitor->visitChildren(this);
}

logoParser::ForeContext* logoParser::fore() {
  ForeContext *_localctx = _tracker.createInstance<ForeContext>(_ctx, getState());
  enterRule(_localctx, 84, logoParser::RuleFore);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(327);
    match(logoParser::T__34);
    setState(328);
    match(logoParser::T__4);
    setState(329);
    name();
    setState(330);
    expression();
    setState(331);
    expression();
    setState(333);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << logoParser::T__2)
      | (1ULL << logoParser::T__31)
      | (1ULL << logoParser::T__32)
      | (1ULL << logoParser::T__33)
      | (1ULL << logoParser::T__35)
      | (1ULL << logoParser::LPAREN)
      | (1ULL << logoParser::MINUS)
      | (1ULL << logoParser::AND)
      | (1ULL << logoParser::OR)
      | (1ULL << logoParser::TRUE)
      | (1ULL << logoParser::FALSE)
      | (1ULL << logoParser::NUMBER))) != 0)) {
      setState(332);
      expression();
    }
    setState(335);
    match(logoParser::T__5);
    setState(336);
    block();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

logoParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::NumberContext::NUMBER() {
  return getToken(logoParser::NUMBER, 0);
}


size_t logoParser::NumberContext::getRuleIndex() const {
  return logoParser::RuleNumber;
}

void logoParser::NumberContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterNumber(this);
}

void logoParser::NumberContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitNumber(this);
}


antlrcpp::Any logoParser::NumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitNumber(this);
  else
    return visitor->visitChildren(this);
}

logoParser::NumberContext* logoParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 86, logoParser::RuleNumber);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(338);
    match(logoParser::NUMBER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CommentContext ------------------------------------------------------------------

logoParser::CommentContext::CommentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::CommentContext::COMMENT() {
  return getToken(logoParser::COMMENT, 0);
}


size_t logoParser::CommentContext::getRuleIndex() const {
  return logoParser::RuleComment;
}

void logoParser::CommentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterComment(this);
}

void logoParser::CommentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitComment(this);
}


antlrcpp::Any logoParser::CommentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitComment(this);
  else
    return visitor->visitChildren(this);
}

logoParser::CommentContext* logoParser::comment() {
  CommentContext *_localctx = _tracker.createInstance<CommentContext>(_ctx, getState());
  enterRule(_localctx, 88, logoParser::RuleComment);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(340);
    match(logoParser::COMMENT);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringliteralContext ------------------------------------------------------------------

logoParser::StringliteralContext::StringliteralContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* logoParser::StringliteralContext::STRING() {
  return getToken(logoParser::STRING, 0);
}


size_t logoParser::StringliteralContext::getRuleIndex() const {
  return logoParser::RuleStringliteral;
}

void logoParser::StringliteralContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStringliteral(this);
}

void logoParser::StringliteralContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<logoListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStringliteral(this);
}


antlrcpp::Any logoParser::StringliteralContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<logoVisitor*>(visitor))
    return parserVisitor->visitStringliteral(this);
  else
    return visitor->visitChildren(this);
}

logoParser::StringliteralContext* logoParser::stringliteral() {
  StringliteralContext *_localctx = _tracker.createInstance<StringliteralContext>(_ctx, getState());
  enterRule(_localctx, 90, logoParser::RuleStringliteral);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(342);
    match(logoParser::T__35);
    setState(343);
    match(logoParser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool logoParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 19: return arithmeticExpressionSempred(dynamic_cast<ArithmeticExpressionContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool logoParser::arithmeticExpressionSempred(ArithmeticExpressionContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 7);
    case 1: return precpred(_ctx, 6);
    case 2: return precpred(_ctx, 5);
    case 3: return precpred(_ctx, 4);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> logoParser::_decisionToDFA;
atn::PredictionContextCache logoParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN logoParser::_atn;
std::vector<uint16_t> logoParser::_serializedATN;

std::vector<std::string> logoParser::_ruleNames = {
  "prog", "line", "cmd", "control", "statement", "procedureInvocation", 
  "procedureDeclaration", "procedureBlock", "parameterDeclarations", "func", 
  "repeat", "block", "ife", "comparisonOperator", "make", "print", "name", 
  "value", "expression", "arithmeticExpression", "arithmeticNegation", "logicalExpression", 
  "comparisonExpression", "compOperator", "atom", "deref", "fd", "bk", "rt", 
  "lt", "cs", "pu", "pd", "ht", "st", "home", "stop", "label", "setxy", 
  "random", "sin", "cos", "fore", "number", "comment", "stringliteral"
};

std::vector<std::string> logoParser::_literalNames = {
  "", "'to'", "'end'", "':'", "'repeat'", "'['", "']'", "'if'", "'make'", 
  "'print'", "'fd'", "'forward'", "'bk'", "'backward'", "'rt'", "'right'", 
  "'lt'", "'left'", "'cs'", "'clearscreen'", "'pu'", "'penup'", "'pd'", 
  "'pendown'", "'ht'", "'hideturtle'", "'st'", "'showturtle'", "'home'", 
  "'stop'", "'label'", "'setxy'", "'random'", "'sin'", "'cos'", "'for'", 
  "'\"'", "'('", "')'", "'+'", "'-'", "'*'", "'/'", "'>'", "'>='", "'<'", 
  "'<='", "'='", "'and'", "'or'", "'not'", "'true'", "'false'"
};

std::vector<std::string> logoParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "LPAREN", "RPAREN", "PLUS", "MINUS", "TIMES", "DIV", "GT", "GET", 
  "LT", "LET", "EQ", "AND", "OR", "NOT", "TRUE", "FALSE", "STRING", "NUMBER", 
  "COMMENT", "EOL", "WS"
};

dfa::Vocabulary logoParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> logoParser::_tokenNames;

logoParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x3b, 0x15c, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x4, 0x2e, 0x9, 0x2e, 0x4, 0x2f, 
    0x9, 0x2f, 0x3, 0x2, 0x5, 0x2, 0x60, 0xa, 0x2, 0x3, 0x2, 0x6, 0x2, 0x63, 
    0xa, 0x2, 0xd, 0x2, 0xe, 0x2, 0x64, 0x3, 0x2, 0x7, 0x2, 0x68, 0xa, 0x2, 
    0xc, 0x2, 0xe, 0x2, 0x6b, 0xb, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x6, 
    0x3, 0x70, 0xa, 0x3, 0xd, 0x3, 0xe, 0x3, 0x71, 0x3, 0x3, 0x5, 0x3, 0x75, 
    0xa, 0x3, 0x5, 0x3, 0x77, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x7f, 0xa, 0x4, 0x5, 0x4, 0x81, 0xa, 
    0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 0x86, 0xa, 0x5, 0x3, 0x6, 
    0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 
    0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x94, 0xa, 0x6, 0x3, 
    0x7, 0x3, 0x7, 0x7, 0x7, 0x98, 0xa, 0x7, 0xc, 0x7, 0xe, 0x7, 0x9b, 0xb, 
    0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x7, 0x8, 0xa0, 0xa, 0x8, 0xc, 0x8, 
    0xe, 0x8, 0xa3, 0xb, 0x8, 0x3, 0x8, 0x5, 0x8, 0xa6, 0xa, 0x8, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x3, 0x9, 0x5, 0x9, 0xac, 0xa, 0x9, 0x3, 0x9, 0x6, 
    0x9, 0xaf, 0xa, 0x9, 0xd, 0x9, 0xe, 0x9, 0xb0, 0x3, 0xa, 0x3, 0xa, 0x3, 
    0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x5, 0xb, 0xb9, 0xa, 0xb, 0x3, 0xc, 
    0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 0x3, 0xd, 0x6, 0xd, 0xc1, 0xa, 
    0xd, 0xd, 0xd, 0xe, 0xd, 0xc2, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 
    0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x5, 0xe, 0xcd, 0xa, 0xe, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x13, 0x3, 0x13, 0x5, 0x13, 0xde, 0xa, 0x13, 0x3, 0x14, 0x3, 0x14, 0x5, 
    0x14, 0xe2, 0xa, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 
    0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 0x15, 0xeb, 0xa, 0x15, 0x3, 0x15, 0x3, 
    0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 
    0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x7, 0x15, 0xf9, 0xa, 0x15, 
    0xc, 0x15, 0xe, 0x15, 0xfc, 0xb, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 
    0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 
    0x3, 0x17, 0x3, 0x17, 0x5, 0x17, 0x110, 0xa, 0x17, 0x3, 0x18, 0x3, 0x18, 
    0x3, 0x18, 0x3, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 
    0x1a, 0x3, 0x1a, 0x5, 0x1a, 0x11c, 0xa, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 
    0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 
    0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 
    0x3, 0x20, 0x3, 0x20, 0x3, 0x21, 0x3, 0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 
    0x23, 0x3, 0x23, 0x3, 0x24, 0x3, 0x24, 0x3, 0x25, 0x3, 0x25, 0x3, 0x26, 
    0x3, 0x26, 0x3, 0x27, 0x3, 0x27, 0x3, 0x28, 0x3, 0x28, 0x3, 0x28, 0x3, 
    0x28, 0x3, 0x29, 0x3, 0x29, 0x3, 0x29, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 
    0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2c, 0x3, 
    0x2c, 0x3, 0x2c, 0x3, 0x2c, 0x5, 0x2c, 0x150, 0xa, 0x2c, 0x3, 0x2c, 
    0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2e, 0x3, 0x2e, 0x3, 
    0x2f, 0x3, 0x2f, 0x3, 0x2f, 0x3, 0x2f, 0x2, 0x3, 0x28, 0x30, 0x2, 0x4, 
    0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 
    0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 
    0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 0x46, 0x48, 0x4a, 0x4c, 0x4e, 
    0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 0x2, 0xc, 0x3, 0x2, 0x2d, 
    0x31, 0x3, 0x2, 0xc, 0xd, 0x3, 0x2, 0xe, 0xf, 0x3, 0x2, 0x10, 0x11, 
    0x3, 0x2, 0x12, 0x13, 0x3, 0x2, 0x14, 0x15, 0x3, 0x2, 0x16, 0x17, 0x3, 
    0x2, 0x18, 0x19, 0x3, 0x2, 0x1a, 0x1b, 0x3, 0x2, 0x1c, 0x1d, 0x2, 0x160, 
    0x2, 0x62, 0x3, 0x2, 0x2, 0x2, 0x4, 0x76, 0x3, 0x2, 0x2, 0x2, 0x6, 0x80, 
    0x3, 0x2, 0x2, 0x2, 0x8, 0x85, 0x3, 0x2, 0x2, 0x2, 0xa, 0x93, 0x3, 0x2, 
    0x2, 0x2, 0xc, 0x95, 0x3, 0x2, 0x2, 0x2, 0xe, 0x9c, 0x3, 0x2, 0x2, 0x2, 
    0x10, 0xae, 0x3, 0x2, 0x2, 0x2, 0x12, 0xb2, 0x3, 0x2, 0x2, 0x2, 0x14, 
    0xb8, 0x3, 0x2, 0x2, 0x2, 0x16, 0xba, 0x3, 0x2, 0x2, 0x2, 0x18, 0xbe, 
    0x3, 0x2, 0x2, 0x2, 0x1a, 0xc6, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xd0, 0x3, 
    0x2, 0x2, 0x2, 0x1e, 0xd2, 0x3, 0x2, 0x2, 0x2, 0x20, 0xd6, 0x3, 0x2, 
    0x2, 0x2, 0x22, 0xd9, 0x3, 0x2, 0x2, 0x2, 0x24, 0xdd, 0x3, 0x2, 0x2, 
    0x2, 0x26, 0xe1, 0x3, 0x2, 0x2, 0x2, 0x28, 0xea, 0x3, 0x2, 0x2, 0x2, 
    0x2a, 0xfd, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x10f, 0x3, 0x2, 0x2, 0x2, 0x2e, 
    0x111, 0x3, 0x2, 0x2, 0x2, 0x30, 0x115, 0x3, 0x2, 0x2, 0x2, 0x32, 0x11b, 
    0x3, 0x2, 0x2, 0x2, 0x34, 0x11d, 0x3, 0x2, 0x2, 0x2, 0x36, 0x120, 0x3, 
    0x2, 0x2, 0x2, 0x38, 0x123, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x126, 0x3, 0x2, 
    0x2, 0x2, 0x3c, 0x129, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x12c, 0x3, 0x2, 0x2, 
    0x2, 0x40, 0x12e, 0x3, 0x2, 0x2, 0x2, 0x42, 0x130, 0x3, 0x2, 0x2, 0x2, 
    0x44, 0x132, 0x3, 0x2, 0x2, 0x2, 0x46, 0x134, 0x3, 0x2, 0x2, 0x2, 0x48, 
    0x136, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x138, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x13a, 
    0x3, 0x2, 0x2, 0x2, 0x4e, 0x13c, 0x3, 0x2, 0x2, 0x2, 0x50, 0x140, 0x3, 
    0x2, 0x2, 0x2, 0x52, 0x143, 0x3, 0x2, 0x2, 0x2, 0x54, 0x146, 0x3, 0x2, 
    0x2, 0x2, 0x56, 0x149, 0x3, 0x2, 0x2, 0x2, 0x58, 0x154, 0x3, 0x2, 0x2, 
    0x2, 0x5a, 0x156, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x158, 0x3, 0x2, 0x2, 0x2, 
    0x5e, 0x60, 0x5, 0x4, 0x3, 0x2, 0x5f, 0x5e, 0x3, 0x2, 0x2, 0x2, 0x5f, 
    0x60, 0x3, 0x2, 0x2, 0x2, 0x60, 0x61, 0x3, 0x2, 0x2, 0x2, 0x61, 0x63, 
    0x7, 0x3a, 0x2, 0x2, 0x62, 0x5f, 0x3, 0x2, 0x2, 0x2, 0x63, 0x64, 0x3, 
    0x2, 0x2, 0x2, 0x64, 0x62, 0x3, 0x2, 0x2, 0x2, 0x64, 0x65, 0x3, 0x2, 
    0x2, 0x2, 0x65, 0x69, 0x3, 0x2, 0x2, 0x2, 0x66, 0x68, 0x5, 0x4, 0x3, 
    0x2, 0x67, 0x66, 0x3, 0x2, 0x2, 0x2, 0x68, 0x6b, 0x3, 0x2, 0x2, 0x2, 
    0x69, 0x67, 0x3, 0x2, 0x2, 0x2, 0x69, 0x6a, 0x3, 0x2, 0x2, 0x2, 0x6a, 
    0x3, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x69, 0x3, 0x2, 0x2, 0x2, 0x6c, 0x77, 
    0x5, 0x5a, 0x2e, 0x2, 0x6d, 0x77, 0x5, 0xe, 0x8, 0x2, 0x6e, 0x70, 0x5, 
    0x6, 0x4, 0x2, 0x6f, 0x6e, 0x3, 0x2, 0x2, 0x2, 0x70, 0x71, 0x3, 0x2, 
    0x2, 0x2, 0x71, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x71, 0x72, 0x3, 0x2, 0x2, 
    0x2, 0x72, 0x74, 0x3, 0x2, 0x2, 0x2, 0x73, 0x75, 0x5, 0x5a, 0x2e, 0x2, 
    0x74, 0x73, 0x3, 0x2, 0x2, 0x2, 0x74, 0x75, 0x3, 0x2, 0x2, 0x2, 0x75, 
    0x77, 0x3, 0x2, 0x2, 0x2, 0x76, 0x6c, 0x3, 0x2, 0x2, 0x2, 0x76, 0x6d, 
    0x3, 0x2, 0x2, 0x2, 0x76, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x77, 0x5, 0x3, 
    0x2, 0x2, 0x2, 0x78, 0x81, 0x5, 0xa, 0x6, 0x2, 0x79, 0x81, 0x5, 0x8, 
    0x5, 0x2, 0x7a, 0x81, 0x5, 0x1e, 0x10, 0x2, 0x7b, 0x81, 0x5, 0xc, 0x7, 
    0x2, 0x7c, 0x7e, 0x5, 0x20, 0x11, 0x2, 0x7d, 0x7f, 0x5, 0x5a, 0x2e, 
    0x2, 0x7e, 0x7d, 0x3, 0x2, 0x2, 0x2, 0x7e, 0x7f, 0x3, 0x2, 0x2, 0x2, 
    0x7f, 0x81, 0x3, 0x2, 0x2, 0x2, 0x80, 0x78, 0x3, 0x2, 0x2, 0x2, 0x80, 
    0x79, 0x3, 0x2, 0x2, 0x2, 0x80, 0x7a, 0x3, 0x2, 0x2, 0x2, 0x80, 0x7b, 
    0x3, 0x2, 0x2, 0x2, 0x80, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x81, 0x7, 0x3, 
    0x2, 0x2, 0x2, 0x82, 0x86, 0x5, 0x16, 0xc, 0x2, 0x83, 0x86, 0x5, 0x1a, 
    0xe, 0x2, 0x84, 0x86, 0x5, 0x56, 0x2c, 0x2, 0x85, 0x82, 0x3, 0x2, 0x2, 
    0x2, 0x85, 0x83, 0x3, 0x2, 0x2, 0x2, 0x85, 0x84, 0x3, 0x2, 0x2, 0x2, 
    0x86, 0x9, 0x3, 0x2, 0x2, 0x2, 0x87, 0x94, 0x5, 0x36, 0x1c, 0x2, 0x88, 
    0x94, 0x5, 0x38, 0x1d, 0x2, 0x89, 0x94, 0x5, 0x3a, 0x1e, 0x2, 0x8a, 
    0x94, 0x5, 0x3c, 0x1f, 0x2, 0x8b, 0x94, 0x5, 0x3e, 0x20, 0x2, 0x8c, 
    0x94, 0x5, 0x40, 0x21, 0x2, 0x8d, 0x94, 0x5, 0x42, 0x22, 0x2, 0x8e, 
    0x94, 0x5, 0x44, 0x23, 0x2, 0x8f, 0x94, 0x5, 0x46, 0x24, 0x2, 0x90, 
    0x94, 0x5, 0x48, 0x25, 0x2, 0x91, 0x94, 0x5, 0x4c, 0x27, 0x2, 0x92, 
    0x94, 0x5, 0x4e, 0x28, 0x2, 0x93, 0x87, 0x3, 0x2, 0x2, 0x2, 0x93, 0x88, 
    0x3, 0x2, 0x2, 0x2, 0x93, 0x89, 0x3, 0x2, 0x2, 0x2, 0x93, 0x8a, 0x3, 
    0x2, 0x2, 0x2, 0x93, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x93, 0x8c, 0x3, 0x2, 
    0x2, 0x2, 0x93, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x93, 0x8e, 0x3, 0x2, 0x2, 
    0x2, 0x93, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x93, 0x90, 0x3, 0x2, 0x2, 0x2, 
    0x93, 0x91, 0x3, 0x2, 0x2, 0x2, 0x93, 0x92, 0x3, 0x2, 0x2, 0x2, 0x94, 
    0xb, 0x3, 0x2, 0x2, 0x2, 0x95, 0x99, 0x5, 0x22, 0x12, 0x2, 0x96, 0x98, 
    0x5, 0x26, 0x14, 0x2, 0x97, 0x96, 0x3, 0x2, 0x2, 0x2, 0x98, 0x9b, 0x3, 
    0x2, 0x2, 0x2, 0x99, 0x97, 0x3, 0x2, 0x2, 0x2, 0x99, 0x9a, 0x3, 0x2, 
    0x2, 0x2, 0x9a, 0xd, 0x3, 0x2, 0x2, 0x2, 0x9b, 0x99, 0x3, 0x2, 0x2, 
    0x2, 0x9c, 0x9d, 0x7, 0x3, 0x2, 0x2, 0x9d, 0xa1, 0x5, 0x22, 0x12, 0x2, 
    0x9e, 0xa0, 0x5, 0x12, 0xa, 0x2, 0x9f, 0x9e, 0x3, 0x2, 0x2, 0x2, 0xa0, 
    0xa3, 0x3, 0x2, 0x2, 0x2, 0xa1, 0x9f, 0x3, 0x2, 0x2, 0x2, 0xa1, 0xa2, 
    0x3, 0x2, 0x2, 0x2, 0xa2, 0xa5, 0x3, 0x2, 0x2, 0x2, 0xa3, 0xa1, 0x3, 
    0x2, 0x2, 0x2, 0xa4, 0xa6, 0x7, 0x3a, 0x2, 0x2, 0xa5, 0xa4, 0x3, 0x2, 
    0x2, 0x2, 0xa5, 0xa6, 0x3, 0x2, 0x2, 0x2, 0xa6, 0xa7, 0x3, 0x2, 0x2, 
    0x2, 0xa7, 0xa8, 0x5, 0x10, 0x9, 0x2, 0xa8, 0xa9, 0x7, 0x4, 0x2, 0x2, 
    0xa9, 0xf, 0x3, 0x2, 0x2, 0x2, 0xaa, 0xac, 0x5, 0x4, 0x3, 0x2, 0xab, 
    0xaa, 0x3, 0x2, 0x2, 0x2, 0xab, 0xac, 0x3, 0x2, 0x2, 0x2, 0xac, 0xad, 
    0x3, 0x2, 0x2, 0x2, 0xad, 0xaf, 0x7, 0x3a, 0x2, 0x2, 0xae, 0xab, 0x3, 
    0x2, 0x2, 0x2, 0xaf, 0xb0, 0x3, 0x2, 0x2, 0x2, 0xb0, 0xae, 0x3, 0x2, 
    0x2, 0x2, 0xb0, 0xb1, 0x3, 0x2, 0x2, 0x2, 0xb1, 0x11, 0x3, 0x2, 0x2, 
    0x2, 0xb2, 0xb3, 0x7, 0x5, 0x2, 0x2, 0xb3, 0xb4, 0x5, 0x22, 0x12, 0x2, 
    0xb4, 0x13, 0x3, 0x2, 0x2, 0x2, 0xb5, 0xb9, 0x5, 0x50, 0x29, 0x2, 0xb6, 
    0xb9, 0x5, 0x52, 0x2a, 0x2, 0xb7, 0xb9, 0x5, 0x54, 0x2b, 0x2, 0xb8, 
    0xb5, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb6, 0x3, 0x2, 0x2, 0x2, 0xb8, 0xb7, 
    0x3, 0x2, 0x2, 0x2, 0xb9, 0x15, 0x3, 0x2, 0x2, 0x2, 0xba, 0xbb, 0x7, 
    0x6, 0x2, 0x2, 0xbb, 0xbc, 0x5, 0x58, 0x2d, 0x2, 0xbc, 0xbd, 0x5, 0x18, 
    0xd, 0x2, 0xbd, 0x17, 0x3, 0x2, 0x2, 0x2, 0xbe, 0xc0, 0x7, 0x7, 0x2, 
    0x2, 0xbf, 0xc1, 0x5, 0x6, 0x4, 0x2, 0xc0, 0xbf, 0x3, 0x2, 0x2, 0x2, 
    0xc1, 0xc2, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc0, 0x3, 0x2, 0x2, 0x2, 0xc2, 
    0xc3, 0x3, 0x2, 0x2, 0x2, 0xc3, 0xc4, 0x3, 0x2, 0x2, 0x2, 0xc4, 0xc5, 
    0x7, 0x8, 0x2, 0x2, 0xc5, 0x19, 0x3, 0x2, 0x2, 0x2, 0xc6, 0xcc, 0x7, 
    0x9, 0x2, 0x2, 0xc7, 0xcd, 0x5, 0x26, 0x14, 0x2, 0xc8, 0xc9, 0x7, 0x7, 
    0x2, 0x2, 0xc9, 0xca, 0x5, 0x26, 0x14, 0x2, 0xca, 0xcb, 0x7, 0x8, 0x2, 
    0x2, 0xcb, 0xcd, 0x3, 0x2, 0x2, 0x2, 0xcc, 0xc7, 0x3, 0x2, 0x2, 0x2, 
    0xcc, 0xc8, 0x3, 0x2, 0x2, 0x2, 0xcd, 0xce, 0x3, 0x2, 0x2, 0x2, 0xce, 
    0xcf, 0x5, 0x18, 0xd, 0x2, 0xcf, 0x1b, 0x3, 0x2, 0x2, 0x2, 0xd0, 0xd1, 
    0x9, 0x2, 0x2, 0x2, 0xd1, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xd2, 0xd3, 0x7, 
    0xa, 0x2, 0x2, 0xd3, 0xd4, 0x5, 0x5c, 0x2f, 0x2, 0xd4, 0xd5, 0x5, 0x24, 
    0x13, 0x2, 0xd5, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xd6, 0xd7, 0x7, 0xb, 0x2, 
    0x2, 0xd7, 0xd8, 0x5, 0x24, 0x13, 0x2, 0xd8, 0x21, 0x3, 0x2, 0x2, 0x2, 
    0xd9, 0xda, 0x7, 0x37, 0x2, 0x2, 0xda, 0x23, 0x3, 0x2, 0x2, 0x2, 0xdb, 
    0xde, 0x5, 0x26, 0x14, 0x2, 0xdc, 0xde, 0x5, 0x34, 0x1b, 0x2, 0xdd, 
    0xdb, 0x3, 0x2, 0x2, 0x2, 0xdd, 0xdc, 0x3, 0x2, 0x2, 0x2, 0xde, 0x25, 
    0x3, 0x2, 0x2, 0x2, 0xdf, 0xe2, 0x5, 0x2c, 0x17, 0x2, 0xe0, 0xe2, 0x5, 
    0x28, 0x15, 0x2, 0xe1, 0xdf, 0x3, 0x2, 0x2, 0x2, 0xe1, 0xe0, 0x3, 0x2, 
    0x2, 0x2, 0xe2, 0x27, 0x3, 0x2, 0x2, 0x2, 0xe3, 0xe4, 0x8, 0x15, 0x1, 
    0x2, 0xe4, 0xeb, 0x5, 0x2a, 0x16, 0x2, 0xe5, 0xe6, 0x7, 0x27, 0x2, 0x2, 
    0xe6, 0xe7, 0x5, 0x28, 0x15, 0x2, 0xe7, 0xe8, 0x7, 0x28, 0x2, 0x2, 0xe8, 
    0xeb, 0x3, 0x2, 0x2, 0x2, 0xe9, 0xeb, 0x5, 0x32, 0x1a, 0x2, 0xea, 0xe3, 
    0x3, 0x2, 0x2, 0x2, 0xea, 0xe5, 0x3, 0x2, 0x2, 0x2, 0xea, 0xe9, 0x3, 
    0x2, 0x2, 0x2, 0xeb, 0xfa, 0x3, 0x2, 0x2, 0x2, 0xec, 0xed, 0xc, 0x9, 
    0x2, 0x2, 0xed, 0xee, 0x7, 0x2b, 0x2, 0x2, 0xee, 0xf9, 0x5, 0x28, 0x15, 
    0xa, 0xef, 0xf0, 0xc, 0x8, 0x2, 0x2, 0xf0, 0xf1, 0x7, 0x2c, 0x2, 0x2, 
    0xf1, 0xf9, 0x5, 0x28, 0x15, 0x9, 0xf2, 0xf3, 0xc, 0x7, 0x2, 0x2, 0xf3, 
    0xf4, 0x7, 0x29, 0x2, 0x2, 0xf4, 0xf9, 0x5, 0x28, 0x15, 0x8, 0xf5, 0xf6, 
    0xc, 0x6, 0x2, 0x2, 0xf6, 0xf7, 0x7, 0x2a, 0x2, 0x2, 0xf7, 0xf9, 0x5, 
    0x28, 0x15, 0x7, 0xf8, 0xec, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xef, 0x3, 0x2, 
    0x2, 0x2, 0xf8, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xf5, 0x3, 0x2, 0x2, 
    0x2, 0xf9, 0xfc, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xf8, 0x3, 0x2, 0x2, 0x2, 
    0xfa, 0xfb, 0x3, 0x2, 0x2, 0x2, 0xfb, 0x29, 0x3, 0x2, 0x2, 0x2, 0xfc, 
    0xfa, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xfe, 0x7, 0x2a, 0x2, 0x2, 0xfe, 0xff, 
    0x5, 0x28, 0x15, 0x2, 0xff, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x100, 0x101, 
    0x7, 0x32, 0x2, 0x2, 0x101, 0x102, 0x5, 0x2c, 0x17, 0x2, 0x102, 0x103, 
    0x5, 0x2c, 0x17, 0x2, 0x103, 0x110, 0x3, 0x2, 0x2, 0x2, 0x104, 0x105, 
    0x7, 0x33, 0x2, 0x2, 0x105, 0x106, 0x5, 0x2c, 0x17, 0x2, 0x106, 0x107, 
    0x5, 0x2c, 0x17, 0x2, 0x107, 0x110, 0x3, 0x2, 0x2, 0x2, 0x108, 0x110, 
    0x5, 0x2e, 0x18, 0x2, 0x109, 0x10a, 0x7, 0x27, 0x2, 0x2, 0x10a, 0x10b, 
    0x5, 0x2c, 0x17, 0x2, 0x10b, 0x10c, 0x7, 0x28, 0x2, 0x2, 0x10c, 0x110, 
    0x3, 0x2, 0x2, 0x2, 0x10d, 0x110, 0x7, 0x35, 0x2, 0x2, 0x10e, 0x110, 
    0x7, 0x36, 0x2, 0x2, 0x10f, 0x100, 0x3, 0x2, 0x2, 0x2, 0x10f, 0x104, 
    0x3, 0x2, 0x2, 0x2, 0x10f, 0x108, 0x3, 0x2, 0x2, 0x2, 0x10f, 0x109, 
    0x3, 0x2, 0x2, 0x2, 0x10f, 0x10d, 0x3, 0x2, 0x2, 0x2, 0x10f, 0x10e, 
    0x3, 0x2, 0x2, 0x2, 0x110, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x111, 0x112, 0x5, 
    0x28, 0x15, 0x2, 0x112, 0x113, 0x5, 0x30, 0x19, 0x2, 0x113, 0x114, 0x5, 
    0x28, 0x15, 0x2, 0x114, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x115, 0x116, 0x9, 
    0x2, 0x2, 0x2, 0x116, 0x31, 0x3, 0x2, 0x2, 0x2, 0x117, 0x11c, 0x5, 0x58, 
    0x2d, 0x2, 0x118, 0x11c, 0x5, 0x34, 0x1b, 0x2, 0x119, 0x11c, 0x5, 0x14, 
    0xb, 0x2, 0x11a, 0x11c, 0x5, 0x5c, 0x2f, 0x2, 0x11b, 0x117, 0x3, 0x2, 
    0x2, 0x2, 0x11b, 0x118, 0x3, 0x2, 0x2, 0x2, 0x11b, 0x119, 0x3, 0x2, 
    0x2, 0x2, 0x11b, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x11c, 0x33, 0x3, 0x2, 0x2, 
    0x2, 0x11d, 0x11e, 0x7, 0x5, 0x2, 0x2, 0x11e, 0x11f, 0x5, 0x22, 0x12, 
    0x2, 0x11f, 0x35, 0x3, 0x2, 0x2, 0x2, 0x120, 0x121, 0x9, 0x3, 0x2, 0x2, 
    0x121, 0x122, 0x5, 0x26, 0x14, 0x2, 0x122, 0x37, 0x3, 0x2, 0x2, 0x2, 
    0x123, 0x124, 0x9, 0x4, 0x2, 0x2, 0x124, 0x125, 0x5, 0x26, 0x14, 0x2, 
    0x125, 0x39, 0x3, 0x2, 0x2, 0x2, 0x126, 0x127, 0x9, 0x5, 0x2, 0x2, 0x127, 
    0x128, 0x5, 0x26, 0x14, 0x2, 0x128, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x129, 
    0x12a, 0x9, 0x6, 0x2, 0x2, 0x12a, 0x12b, 0x5, 0x26, 0x14, 0x2, 0x12b, 
    0x3d, 0x3, 0x2, 0x2, 0x2, 0x12c, 0x12d, 0x9, 0x7, 0x2, 0x2, 0x12d, 0x3f, 
    0x3, 0x2, 0x2, 0x2, 0x12e, 0x12f, 0x9, 0x8, 0x2, 0x2, 0x12f, 0x41, 0x3, 
    0x2, 0x2, 0x2, 0x130, 0x131, 0x9, 0x9, 0x2, 0x2, 0x131, 0x43, 0x3, 0x2, 
    0x2, 0x2, 0x132, 0x133, 0x9, 0xa, 0x2, 0x2, 0x133, 0x45, 0x3, 0x2, 0x2, 
    0x2, 0x134, 0x135, 0x9, 0xb, 0x2, 0x2, 0x135, 0x47, 0x3, 0x2, 0x2, 0x2, 
    0x136, 0x137, 0x7, 0x1e, 0x2, 0x2, 0x137, 0x49, 0x3, 0x2, 0x2, 0x2, 
    0x138, 0x139, 0x7, 0x1f, 0x2, 0x2, 0x139, 0x4b, 0x3, 0x2, 0x2, 0x2, 
    0x13a, 0x13b, 0x7, 0x20, 0x2, 0x2, 0x13b, 0x4d, 0x3, 0x2, 0x2, 0x2, 
    0x13c, 0x13d, 0x7, 0x21, 0x2, 0x2, 0x13d, 0x13e, 0x5, 0x26, 0x14, 0x2, 
    0x13e, 0x13f, 0x5, 0x26, 0x14, 0x2, 0x13f, 0x4f, 0x3, 0x2, 0x2, 0x2, 
    0x140, 0x141, 0x7, 0x22, 0x2, 0x2, 0x141, 0x142, 0x5, 0x26, 0x14, 0x2, 
    0x142, 0x51, 0x3, 0x2, 0x2, 0x2, 0x143, 0x144, 0x7, 0x23, 0x2, 0x2, 
    0x144, 0x145, 0x5, 0x28, 0x15, 0x2, 0x145, 0x53, 0x3, 0x2, 0x2, 0x2, 
    0x146, 0x147, 0x7, 0x24, 0x2, 0x2, 0x147, 0x148, 0x5, 0x28, 0x15, 0x2, 
    0x148, 0x55, 0x3, 0x2, 0x2, 0x2, 0x149, 0x14a, 0x7, 0x25, 0x2, 0x2, 
    0x14a, 0x14b, 0x7, 0x7, 0x2, 0x2, 0x14b, 0x14c, 0x5, 0x22, 0x12, 0x2, 
    0x14c, 0x14d, 0x5, 0x26, 0x14, 0x2, 0x14d, 0x14f, 0x5, 0x26, 0x14, 0x2, 
    0x14e, 0x150, 0x5, 0x26, 0x14, 0x2, 0x14f, 0x14e, 0x3, 0x2, 0x2, 0x2, 
    0x14f, 0x150, 0x3, 0x2, 0x2, 0x2, 0x150, 0x151, 0x3, 0x2, 0x2, 0x2, 
    0x151, 0x152, 0x7, 0x8, 0x2, 0x2, 0x152, 0x153, 0x5, 0x18, 0xd, 0x2, 
    0x153, 0x57, 0x3, 0x2, 0x2, 0x2, 0x154, 0x155, 0x7, 0x38, 0x2, 0x2, 
    0x155, 0x59, 0x3, 0x2, 0x2, 0x2, 0x156, 0x157, 0x7, 0x39, 0x2, 0x2, 
    0x157, 0x5b, 0x3, 0x2, 0x2, 0x2, 0x158, 0x159, 0x7, 0x26, 0x2, 0x2, 
    0x159, 0x15a, 0x7, 0x37, 0x2, 0x2, 0x15a, 0x5d, 0x3, 0x2, 0x2, 0x2, 
    0x1c, 0x5f, 0x64, 0x69, 0x71, 0x74, 0x76, 0x7e, 0x80, 0x85, 0x93, 0x99, 
    0xa1, 0xa5, 0xab, 0xb0, 0xb8, 0xc2, 0xcc, 0xdd, 0xe1, 0xea, 0xf8, 0xfa, 
    0x10f, 0x11b, 0x14f, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

logoParser::Initializer logoParser::_init;
