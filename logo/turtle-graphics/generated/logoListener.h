
// Generated from /home/usinkevi/U/tkom/tkom.-logo/logo/turtle-graphics/logo.g4 by ANTLR 4.7.1

#pragma once


#include "antlr4-runtime.h"
#include "logoParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by logoParser.
 */
class  logoListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterProg(logoParser::ProgContext *ctx) = 0;
  virtual void exitProg(logoParser::ProgContext *ctx) = 0;

  virtual void enterLine(logoParser::LineContext *ctx) = 0;
  virtual void exitLine(logoParser::LineContext *ctx) = 0;

  virtual void enterCmd(logoParser::CmdContext *ctx) = 0;
  virtual void exitCmd(logoParser::CmdContext *ctx) = 0;

  virtual void enterControl(logoParser::ControlContext *ctx) = 0;
  virtual void exitControl(logoParser::ControlContext *ctx) = 0;

  virtual void enterStatement(logoParser::StatementContext *ctx) = 0;
  virtual void exitStatement(logoParser::StatementContext *ctx) = 0;

  virtual void enterProcedureInvocation(logoParser::ProcedureInvocationContext *ctx) = 0;
  virtual void exitProcedureInvocation(logoParser::ProcedureInvocationContext *ctx) = 0;

  virtual void enterProcedureDeclaration(logoParser::ProcedureDeclarationContext *ctx) = 0;
  virtual void exitProcedureDeclaration(logoParser::ProcedureDeclarationContext *ctx) = 0;

  virtual void enterProcedureBlock(logoParser::ProcedureBlockContext *ctx) = 0;
  virtual void exitProcedureBlock(logoParser::ProcedureBlockContext *ctx) = 0;

  virtual void enterParameterDeclarations(logoParser::ParameterDeclarationsContext *ctx) = 0;
  virtual void exitParameterDeclarations(logoParser::ParameterDeclarationsContext *ctx) = 0;

  virtual void enterFunc(logoParser::FuncContext *ctx) = 0;
  virtual void exitFunc(logoParser::FuncContext *ctx) = 0;

  virtual void enterRepeat(logoParser::RepeatContext *ctx) = 0;
  virtual void exitRepeat(logoParser::RepeatContext *ctx) = 0;

  virtual void enterBlock(logoParser::BlockContext *ctx) = 0;
  virtual void exitBlock(logoParser::BlockContext *ctx) = 0;

  virtual void enterIfe(logoParser::IfeContext *ctx) = 0;
  virtual void exitIfe(logoParser::IfeContext *ctx) = 0;

  virtual void enterComparisonOperator(logoParser::ComparisonOperatorContext *ctx) = 0;
  virtual void exitComparisonOperator(logoParser::ComparisonOperatorContext *ctx) = 0;

  virtual void enterMake(logoParser::MakeContext *ctx) = 0;
  virtual void exitMake(logoParser::MakeContext *ctx) = 0;

  virtual void enterPrint(logoParser::PrintContext *ctx) = 0;
  virtual void exitPrint(logoParser::PrintContext *ctx) = 0;

  virtual void enterName(logoParser::NameContext *ctx) = 0;
  virtual void exitName(logoParser::NameContext *ctx) = 0;

  virtual void enterValue(logoParser::ValueContext *ctx) = 0;
  virtual void exitValue(logoParser::ValueContext *ctx) = 0;

  virtual void enterExpression(logoParser::ExpressionContext *ctx) = 0;
  virtual void exitExpression(logoParser::ExpressionContext *ctx) = 0;

  virtual void enterArithmeticExpression(logoParser::ArithmeticExpressionContext *ctx) = 0;
  virtual void exitArithmeticExpression(logoParser::ArithmeticExpressionContext *ctx) = 0;

  virtual void enterArithmeticNegation(logoParser::ArithmeticNegationContext *ctx) = 0;
  virtual void exitArithmeticNegation(logoParser::ArithmeticNegationContext *ctx) = 0;

  virtual void enterLogicalExpression(logoParser::LogicalExpressionContext *ctx) = 0;
  virtual void exitLogicalExpression(logoParser::LogicalExpressionContext *ctx) = 0;

  virtual void enterComparisonExpression(logoParser::ComparisonExpressionContext *ctx) = 0;
  virtual void exitComparisonExpression(logoParser::ComparisonExpressionContext *ctx) = 0;

  virtual void enterCompOperator(logoParser::CompOperatorContext *ctx) = 0;
  virtual void exitCompOperator(logoParser::CompOperatorContext *ctx) = 0;

  virtual void enterAtom(logoParser::AtomContext *ctx) = 0;
  virtual void exitAtom(logoParser::AtomContext *ctx) = 0;

  virtual void enterDeref(logoParser::DerefContext *ctx) = 0;
  virtual void exitDeref(logoParser::DerefContext *ctx) = 0;

  virtual void enterFd(logoParser::FdContext *ctx) = 0;
  virtual void exitFd(logoParser::FdContext *ctx) = 0;

  virtual void enterBk(logoParser::BkContext *ctx) = 0;
  virtual void exitBk(logoParser::BkContext *ctx) = 0;

  virtual void enterRt(logoParser::RtContext *ctx) = 0;
  virtual void exitRt(logoParser::RtContext *ctx) = 0;

  virtual void enterLt(logoParser::LtContext *ctx) = 0;
  virtual void exitLt(logoParser::LtContext *ctx) = 0;

  virtual void enterCs(logoParser::CsContext *ctx) = 0;
  virtual void exitCs(logoParser::CsContext *ctx) = 0;

  virtual void enterPu(logoParser::PuContext *ctx) = 0;
  virtual void exitPu(logoParser::PuContext *ctx) = 0;

  virtual void enterPd(logoParser::PdContext *ctx) = 0;
  virtual void exitPd(logoParser::PdContext *ctx) = 0;

  virtual void enterHt(logoParser::HtContext *ctx) = 0;
  virtual void exitHt(logoParser::HtContext *ctx) = 0;

  virtual void enterSt(logoParser::StContext *ctx) = 0;
  virtual void exitSt(logoParser::StContext *ctx) = 0;

  virtual void enterHome(logoParser::HomeContext *ctx) = 0;
  virtual void exitHome(logoParser::HomeContext *ctx) = 0;

  virtual void enterStop(logoParser::StopContext *ctx) = 0;
  virtual void exitStop(logoParser::StopContext *ctx) = 0;

  virtual void enterLabel(logoParser::LabelContext *ctx) = 0;
  virtual void exitLabel(logoParser::LabelContext *ctx) = 0;

  virtual void enterSetxy(logoParser::SetxyContext *ctx) = 0;
  virtual void exitSetxy(logoParser::SetxyContext *ctx) = 0;

  virtual void enterRandom(logoParser::RandomContext *ctx) = 0;
  virtual void exitRandom(logoParser::RandomContext *ctx) = 0;

  virtual void enterSin(logoParser::SinContext *ctx) = 0;
  virtual void exitSin(logoParser::SinContext *ctx) = 0;

  virtual void enterCos(logoParser::CosContext *ctx) = 0;
  virtual void exitCos(logoParser::CosContext *ctx) = 0;

  virtual void enterFore(logoParser::ForeContext *ctx) = 0;
  virtual void exitFore(logoParser::ForeContext *ctx) = 0;

  virtual void enterNumber(logoParser::NumberContext *ctx) = 0;
  virtual void exitNumber(logoParser::NumberContext *ctx) = 0;

  virtual void enterComment(logoParser::CommentContext *ctx) = 0;
  virtual void exitComment(logoParser::CommentContext *ctx) = 0;

  virtual void enterStringliteral(logoParser::StringliteralContext *ctx) = 0;
  virtual void exitStringliteral(logoParser::StringliteralContext *ctx) = 0;


};

