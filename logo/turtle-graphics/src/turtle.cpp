#include "turtle.h"

Turtle::Turtle()
  : x(0)
  , y(0)
  , direction(0)
  , color(Qt::GlobalColor::gray)
{
  setScale(0.5);
}

QRectF
Turtle::boundingRect() const
{
  qreal adjust = 0.5;
  return QRectF(-18 - adjust, -22 - adjust, 36 + adjust, 60 + adjust);
}

QPainterPath
Turtle::shape() const
{
  QPainterPath path;
  path.addRect(-10, -20, 20, 40);
  return path;
}

void
Turtle::paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*)
{
  // Body
  painter->setBrush(color);
  painter->drawEllipse(-10, -20, 20, 40);

  // Eyes
  painter->setBrush(Qt::white);
  painter->drawEllipse(-10, -17, 8, 8);
  painter->drawEllipse(2, -17, 8, 8);

  // Nose
  painter->setBrush(Qt::black);
  painter->drawEllipse(QRectF(-2, -22, 4, 4));

  // Pupils
  painter->drawEllipse(QRectF(-8.0 + direction, -17, 4, 4));
  painter->drawEllipse(QRectF(4.0 + direction, -17, 4, 4));

  // Ears
  painter->setBrush(scene()->collidingItems(this).isEmpty() ? Qt::darkYellow
                                                            : Qt::red);
  painter->drawEllipse(-17, -12, 16, 16);
  painter->drawEllipse(1, -12, 16, 16);

  // Tail
  QPainterPath path(QPointF(0, 20));
  path.cubicTo(-5, 22, -5, 22, 0, 25);
  path.cubicTo(5, 27, 5, 32, 0, 30);
  path.cubicTo(-5, 32, -5, 42, 0, 35);
  painter->setBrush(Qt::NoBrush);
  painter->drawPath(path);
}

void
Turtle::forward(double dx)
{
  setPos(mapToParent(0, -dx));
  //    x -= dx;
  //  update();
  this->scene()->addLine(QLineF(mapToParent(0, 0), mapToParent(0, dx)));
}

void
Turtle::back(double dx)
{
  setPos(mapToParent(0, dx));
  this->scene()->addLine(QLineF(mapToParent(0, 0), mapToParent(0, -dx)));
}

void
Turtle::left(double dx)
{
  setRotation(rotation() - dx);
}

void
Turtle::right(double dx)
{
  setRotation(rotation() + dx);
}

void
Turtle::clearscreen()
{
  QGraphicsScene* scene = this->scene();
  scene->clear();
  //  scene->addItem()
}

void
Turtle::print(std::string text) const
{
  std::cout << text << std::endl;
}

void
Turtle::print(const double num) const
{
  std::cout << std::to_string(num) << std::endl;
}

double
Turtle::getX() const
{
  return x;
}

double
Turtle::getY() const
{
  return y;
}
