#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <fstream>

#include <QErrorMessage>
#include <QFileDialog>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <memory>

#include "codeeditor.h"
#include "logovisitor.h"
#include "turtle.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow();

private Q_SLOTS:
  void on_clear_clicked();
  void on_run_clicked();

  void on_actionOpen_triggered();
  void on_actionSave_triggered();

private:
  void populateScene();
  void updateUi();
  void errorMessage(const std::string& err) const;
  void informationMessage(const std::string& err) const;
  QString getOpenFileName();
  QString getSaveFileName();

  std::shared_ptr<Ui::MainWindow> ui;
  std::shared_ptr<QGraphicsScene> scene;

  QErrorMessage* errorMessageDialog;
  CodeEditor editor;

  //  std::shared_ptr<Turtle> turtle;
  Turtle* turtle;
};

#endif // MAINWINDOW_H
