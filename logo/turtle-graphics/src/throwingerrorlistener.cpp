#include "throwingerrorlistener.h"

#include <Exceptions.h>

ThrowingErrorListener::ThrowingErrorListener() {}

void
ThrowingErrorListener::syntaxError(antlr4::Recognizer* recognizer,
                                   antlr4::Token* offendingSymbol,
                                   size_t line,
                                   size_t charPositionInLine,
                                   const std::string& msg,
                                   std::__exception_ptr::exception_ptr e)
{
  if (msg != "missing EOL at '<EOF>'")
    throw antlr4::ParseCancellationException(
      "In line " + std::to_string(line) + ":" +
      std::to_string(charPositionInLine) + "\n" + msg);
}
