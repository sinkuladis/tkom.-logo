#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <memory>
#include <unordered_map>
#include <variant>

#include "generated/logoParser.h"

using object =
  std::variant<double, std::string, logoParser::ProcedureDeclarationContext*>;

class Environment
{
public:
  Environment();
  Environment(std::shared_ptr<Environment> enclosing);

  object get(const std::string) const;
  void assign(const std::string name, const object value);
  void define(const std::string name, const object value);

private:
  std::shared_ptr<Environment> m_enclosing;
  std::unordered_map<std::string, object> m_values;
};

#endif // ENVIRONMENT_H
