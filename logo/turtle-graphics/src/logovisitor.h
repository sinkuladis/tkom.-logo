#ifndef LOGOVISITOR_H
#define LOGOVISITOR_H

#include "generated/logoBaseVisitor.h"
#include "generated/logoLexer.h"
#include "generated/logoParser.h"

#include "environment.h"
//#include "logoprocedure.h"
#include "statement.h"
#include "throwingerrorlistener.h"
#include "turtle.h"

#include <random>

class LogoVisitor : public logoBaseVisitor
{
public:
  //  LogoVisitor(std::shared_ptr<Turtle> turtle);
  LogoVisitor(Turtle* turtle);

  void run(const std::string& text) const;

  std::shared_ptr<Environment> globals() const;

  antlrcpp::Any visitProcedureBlock(
    logoParser::ProcedureBlockContext* context) override;

private:
  std::shared_ptr<ThrowingErrorListener> errorListener;

  //  std::shared_ptr<Turtle> m_turtle;
  Turtle* m_turtle;
  std::shared_ptr<Environment> m_globals;
  std::shared_ptr<Environment> m_environment;

  std::random_device rd;
  std::mt19937 gen;
  std::uniform_int_distribution<> distr;

  std::string getLineStr(antlr4::ParserRuleContext* context) const;
  std::string getPositionInLineStr(antlr4::ParserRuleContext* context) const;

  antlrcpp::Any visitStatement(logoParser::StatementContext* context) override;
  antlrcpp::Any visitExpression(
    logoParser::ExpressionContext* context) override;

  antlrcpp::Any visitArithmeticExpression(
    logoParser::ArithmeticExpressionContext* context) override;
  antlrcpp::Any visitArithmeticNegation(
    logoParser::ArithmeticNegationContext* context) override;
  antlrcpp::Any visitAtom(logoParser::AtomContext* context) override;
  antlrcpp::Any visitLogicalExpression(
    logoParser::LogicalExpressionContext* context) override;
  antlrcpp::Any visitComparisonExpression(
    logoParser::ComparisonExpressionContext* context) override;

  antlrcpp::Any visitFd(logoParser::FdContext* context) override;
  antlrcpp::Any visitBk(logoParser::BkContext* context) override;
  antlrcpp::Any visitRt(logoParser::RtContext* context) override;
  antlrcpp::Any visitLt(logoParser::LtContext* context) override;
  antlrcpp::Any visitCs(logoParser::CsContext* context) override;
  antlrcpp::Any visitPu(logoParser::PuContext* context) override;
  antlrcpp::Any visitPd(logoParser::PdContext* context) override;
  antlrcpp::Any visitHt(logoParser::HtContext* context) override;
  antlrcpp::Any visitSt(logoParser::StContext* context) override;
  antlrcpp::Any visitHome(logoParser::HomeContext* context) override;
  antlrcpp::Any visitLabel(logoParser::LabelContext* context) override;
  antlrcpp::Any visitSetxy(logoParser::SetxyContext* context) override;

  antlrcpp::Any visitPrint(logoParser::PrintContext* context) override;

  antlrcpp::Any visitValue(logoParser::ValueContext* context) override;

  antlrcpp::Any visitStringliteral(
    logoParser::StringliteralContext* context) override;

  antlrcpp::Any visitMake(logoParser::MakeContext* context) override;

  antlrcpp::Any visitDeref(logoParser::DerefContext* context) override;

  antlrcpp::Any visitControl(logoParser::ControlContext* context) override;

  antlrcpp::Any visitRepeat(logoParser::RepeatContext* context) override;

  antlrcpp::Any visitIfe(logoParser::IfeContext* context) override;

  antlrcpp::Any visitBlock(logoParser::BlockContext* context) override;

  antlrcpp::Any visitFore(logoParser::ForeContext* context) override;

  antlrcpp::Any visitFunc(logoParser::FuncContext* context) override;

  antlrcpp::Any visitSin(logoParser::SinContext* context) override;
  antlrcpp::Any visitCos(logoParser::CosContext* context) override;
  antlrcpp::Any visitRandom(logoParser::RandomContext* context) override;

  antlrcpp::Any visitProcedureDeclaration(
    logoParser::ProcedureDeclarationContext* context) override;
  antlrcpp::Any visitProcedureInvocation(
    logoParser::ProcedureInvocationContext* context) override;
};

#endif // LOGOVISITOR_H
