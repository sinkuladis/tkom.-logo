#ifndef COMMAND_H
#define COMMAND_H

#include <map>
#include <stdexcept>
#include <utility>

#include <QGraphicsView>
#include <QString>

#include "turtle.h"

const constexpr int NONE = 0;

enum Command
{
  forward,
  back,
  left,
  right,
  home,
  clearscreen,
  setpos,
  print,
  empty_
};

typedef std::map<QString, Command> stringmap;
static stringmap string_command_map = {
  { "forward", Command::forward }, { "back", Command::back },
  { "left", Command::left },       { "right", Command::right },
  { "home", Command::home },       { "clearscreen", Command::clearscreen },
  { "setpos", Command::setpos },   { "print", Command::print },
  { "", Command::empty_ }
};

class Statement
{
public:
  Statement(const QString& command, const QString& value, int lineNum = 0);
  Statement(const QString& command, double value, int lineNum = 0);
  Statement();

  void execute(Turtle& turtle) const;

  std::pair<Command, double> getStatement() const;
  void setStatement(const std::pair<Command, double>& value);
  void setStatement(Command, double value);

private:
  std::pair<Command, double> statement;
  QString str;
};

#endif // COMMAND_H
