#ifndef TURTLE_H
#define TURTLE_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPainter>

#include <iostream>

class Turtle : public QGraphicsItem
{
public:
  Turtle();

  QRectF boundingRect() const;
  QPainterPath shape() const;
  void paint(QPainter* painter,
             const QStyleOptionGraphicsItem* option,
             QWidget* widget);

  void forward(const double dx);
  void back(const double dx);
  void left(const double dphi);
  void right(const double dphi);
  void clearscreen();
  void print(const std::string) const;
  void print(const double) const;

  double getX() const;
  double getY() const;

private:
  double x;
  double y;
  double direction;
  QColor color;
};

#endif // TURTLE_H
