#ifndef LOGOPROCEDURE_H
#define LOGOPROCEDURE_H

//#include "environment.h"
//#include "logovisitor.h"

#include "logovisitor.h"

#include <generated/logoParser.h>

class LogoProcedure
{
public:
  LogoProcedure(logoParser::ProcedureDeclarationContext* context);

  size_t arity();
  void call(LogoVisitor visitor, std::vector<object> arguments);

private:
  logoParser::ProcedureDeclarationContext* m_context;
  std::vector<logoParser::ParameterDeclarationsContext*> m_argumentsNames;
};

#endif // LOGOPROCEDURE_H
