#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "antlr4-runtime.h"
#include "environment.h"
#include "generated/logoLexer.h"
#include "generated/logoParser.h"
#include "turtle.h"

#include <QString>
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

class Interpreter {
public:
  Interpreter(shared_ptr<Turtle> turtle);

  void tokenize(const string &text) const;
  void ast(const string &text) const;
  void run(const string &text) const;

private:
  shared_ptr<Turtle> m_turtle;
};

#endif // INTERPRETER_H
