#include "logovisitor.h"
#include "logoprocedure.h"
#include "statement_error.h"

#include "spdlog/spdlog.h"

// LogoVisitor::LogoVisitor(std::shared_ptr<Turtle> turtle)
LogoVisitor::LogoVisitor(Turtle* turtle)
  : gen(rd())
  , distr(0, 1)
{
  m_turtle = turtle;
  m_globals = std::make_unique<Environment>();
  m_environment = m_globals;
  errorListener = std::make_shared<ThrowingErrorListener>();
}

void
LogoVisitor::run(const std::string& text) const
{
  std::istringstream modelicaFile(text);
  antlr4::ANTLRInputStream input(modelicaFile);
  logoLexer lexer(&input);

  lexer.removeErrorListener(&antlr4::ConsoleErrorListener().INSTANCE);
  lexer.addErrorListener(errorListener.get());

  antlr4::CommonTokenStream tokens(&lexer);

  logoParser parser(&tokens);

  parser.removeErrorListener(&antlr4::ConsoleErrorListener().INSTANCE);
  parser.addErrorListener(errorListener.get());

  logoParser::ProgContext* tree = parser.prog();

  LogoVisitor visitor(m_turtle);
  visitor.visitProg(tree);
}

std::shared_ptr<Environment>
LogoVisitor::globals() const
{
  return m_globals;
}

antlrcpp::Any
LogoVisitor::visitStatement(logoParser::StatementContext* context)
{
  if (context->bk()) {
    visitBk(context->bk());
  } else if (context->fd()) {
    visitFd(context->fd());
  } else if (context->rt()) {
    visitRt(context->rt());
  } else if (context->lt()) {
    visitLt(context->lt());
  } else if (context->cs()) {
    visitCs(context->cs());
  } else if (context->cs()) {
    visitCs(context->cs());
  } else if (context->pu()) {
    visitPu(context->pu());
  } else if (context->pu()) {
    visitPu(context->pu());
  } else if (context->pd()) {
    visitPd(context->pd());
  } else if (context->ht()) {
    visitHt(context->ht());
  } else if (context->st()) {
    visitSt(context->st());
  } else if (context->home()) {
    visitHome(context->home());
  } else if (context->label()) {
    visitLabel(context->label());
  } else if (context->setxy()) {
    visitSetxy(context->setxy());
  }
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitBk(logoParser::BkContext* context)
{
  double value = visitExpression(context->expression());
  Statement stmt("back", value);
  stmt.execute(*m_turtle);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitFd(logoParser::FdContext* context)
{
  double value = visitExpression(context->expression());
  Statement stmt("forward", value);
  stmt.execute(*m_turtle);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitRt(logoParser::RtContext* context)
{
  double value = visitExpression(context->expression());
  Statement stmt("right", value);
  stmt.execute(*m_turtle);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitLt(logoParser::LtContext* context)
{
  double value = visitExpression(context->expression());
  Statement stmt("left", value);
  stmt.execute(*m_turtle);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitCs(logoParser::CsContext* context)
{
  Statement stmt("clearscreen", NONE);
  stmt.execute(*m_turtle);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitPu(logoParser::PuContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure: pu/penup.");
  //  std::string msg = "Unimplemented procedure: pu/penup";
  //  throw statement_error(msg, context);
}

antlrcpp::Any
LogoVisitor::visitPd(logoParser::PdContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure pd/pendown.");
}

antlrcpp::Any
LogoVisitor::visitHt(logoParser::HtContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure: ht/hideturtle.");
}

antlrcpp::Any
LogoVisitor::visitSt(logoParser::StContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure:st/showturtle.");
}

antlrcpp::Any
LogoVisitor::visitHome(logoParser::HomeContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure: home.");
}

antlrcpp::Any
LogoVisitor::visitLabel(logoParser::LabelContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure: label.");
}

antlrcpp::Any
LogoVisitor::visitSetxy(logoParser::SetxyContext* context)
{
  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUsed unimplemented procedure: setxy.");
}

antlrcpp::Any
LogoVisitor::visitPrint(logoParser::PrintContext* context)
{
  antlrcpp::Any value;
  if (context->value()) {
    value = visitValue(context->value());
  }

  if (value.is<std::string>())
    std::cout << value.as<std::string>() << std::endl;
  else if (value.is<double>())
    std::cout << value.as<double>() << std::endl;
  else if (value.is<bool>())
    std::cout << value.as<bool>() << std::endl;
  else
    std::runtime_error("In line " + getLineStr(context) + ":" +
                       getPositionInLineStr(context) +
                       "\nUnimplemented print type");

  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitValue(logoParser::ValueContext* context)
{
  /*if (context->stringliteral()) {
    return visitStringliteral(context->stringliteral());
  } else */
  if (context->expression()) {
    return visitExpression(context->expression());
  } else if (context->deref()) {
    return visitDeref(context->deref());
  }

  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUnvisited value context");
}

antlrcpp::Any
LogoVisitor::visitStringliteral(logoParser::StringliteralContext* context)
{
  return antlrcpp::Any(context->STRING()->getText());
}

antlrcpp::Any
LogoVisitor::visitMake(logoParser::MakeContext* context)
{
  std::string name = context->stringliteral()->STRING()->getText();
  antlrcpp::Any value = visitValue(context->value());

  if (value.is<std::string>())
    m_environment->define(name, value.as<std::string>());
  else if (value.is<double>())
    m_environment->define(name, value.as<double>());

  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitDeref(logoParser::DerefContext* context)
{
  object value = m_environment->get(context->name()->STRING()->getText());
  if (auto pval = std::get_if<std::string>(&value))
    return antlrcpp::Any(*pval);
  else if (auto pval = std::get_if<double>(&value))
    return antlrcpp::Any(*pval);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitControl(logoParser::ControlContext* context)
{
  if (context->repeat())
    visitRepeat(context->repeat());
  else if (context->ife())
    visitIfe(context->ife());
  else if (context->fore())
    visitFore(context->fore());
  return antlrcpp::Any(0);
}

antlrcpp::Any
LogoVisitor::visitRepeat(logoParser::RepeatContext* context)
{
  int iterations =
    static_cast<int>(std::stod(context->number()->NUMBER()->getText()));
  for (int i = 0; i < iterations; ++i) {
    visitBlock(context->block());
  }
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitIfe(logoParser::IfeContext* context)
{
  bool execute = false;
  antlrcpp::Any isTruthy = visitExpression(context->expression());
  if (isTruthy.is<bool>())
    execute = isTruthy.as<bool>();
  else if (isTruthy.isNull())
    execute = false;
  else
    execute = true;

  if (execute)
    visitBlock(context->block());

  return antlrcpp::Any(execute);
}

antlrcpp::Any
LogoVisitor::visitBlock(logoParser::BlockContext* context)
{
  visitChildren(context);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitFore(logoParser::ForeContext* context)
{
  std::string name = context->name()->STRING()->getText();
  antlrcpp::Any start = visitExpression(context->expression()[0]);
  antlrcpp::Any end = visitExpression(context->expression()[1]);
  int iterationStart = static_cast<int>(start.as<double>());
  int iterationEnd = static_cast<int>(end.as<double>());
  int iterationStep = 1;

  if (context->expression().size() >= 3) {
    iterationStep =
      static_cast<int>(visitExpression(context->expression()[2]).as<double>());
  }
  std::shared_ptr<Environment> previous = this->m_environment;
  this->m_environment = std::make_shared<Environment>(previous);
  m_environment->define(name, iterationStart);
  for (int i = iterationStart; i <= iterationEnd; i += iterationStep) {
    m_environment->assign(name, i);
    visitBlock(context->block());
  }

  this->m_environment = previous;
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitFunc(logoParser::FuncContext* context)
{
  if (context->random())
    return visitRandom(context->random());
  else if (context->sin())
    return visitSin(context->sin());
  else if (context->cos())
    return visitCos(context->cos());

  throw std::runtime_error("In line " + getLineStr(context) + ":" +
                           getPositionInLineStr(context) +
                           "\nUnimplemented rule in visitFunc");
}

antlrcpp::Any
LogoVisitor::visitSin(logoParser::SinContext* context)
{
  antlrcpp::Any value =
    visitArithmeticExpression(context->arithmeticExpression());
  double num = value.as<double>();
  return antlrcpp::Any(std::sin(num * M_PI / 180));
}

antlrcpp::Any
LogoVisitor::visitCos(logoParser::CosContext* context)
{
  antlrcpp::Any value =
    visitArithmeticExpression(context->arithmeticExpression());
  double num = value.as<double>();
  return antlrcpp::Any(std::cos(num));
}

antlrcpp::Any
LogoVisitor::visitRandom(logoParser::RandomContext* context)
{
  antlrcpp::Any value = visitExpression(context->expression());
  int num = static_cast<int>(value.as<double>());
  distr = std::uniform_int_distribution<>(0, num);
  return antlrcpp::Any(static_cast<double>(distr(gen)));
}

antlrcpp::Any
LogoVisitor::visitProcedureDeclaration(
  logoParser::ProcedureDeclarationContext* context)
{
  if (context->parameterDeclarations().size() > 8)
    throw std::runtime_error(
      "In line " + getLineStr(context) + ":" + getPositionInLineStr(context) +
      "\nCannot have more than 8 arguments in function '" +
      context->name()->STRING()->getText() + "'");

  m_environment->define(context->name()->STRING()->getText(), context);
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitProcedureInvocation(
  logoParser::ProcedureInvocationContext* context)
{
  object callable = object();
  try {
    object callable = m_environment->get(context->name()->getText());
  } catch (std::exception& e) {
    throw std::runtime_error("In line " + getLineStr(context) + ":" +
                             getPositionInLineStr(context) + "\n" + e.what());
  }

  if (!std::get_if<logoParser::ProcedureDeclarationContext*>(&callable))
    throw std::runtime_error(
      "In line " + getLineStr(context) + ":" + getPositionInLineStr(context) +
      "\nProcedure invocation error: invalid variable context");

  logoParser::ProcedureDeclarationContext* procContext =
    *(std::get_if<logoParser::ProcedureDeclarationContext*>(&callable));

  if (procContext->parameterDeclarations().size() !=
      context->expression().size())
    throw std::runtime_error(
      "In line " + getLineStr(context) + ":" + getPositionInLineStr(context) +
      "\nExpected " +
      std::to_string(procContext->parameterDeclarations().size()) +
      " arguements in function '" + context->name()->STRING()->getText() + "'");

  std::shared_ptr<Environment> procEnv = m_globals;

  int argCount = context->expression().size();

  for (int i = 0; i < argCount; ++i) {
    std::string argName =
      procContext->parameterDeclarations()[i]->name()->STRING()->getText();

    object argValue;
    antlrcpp::Any exprResult = visitExpression(context->expression()[i]);

    if (exprResult.is<double>())
      argValue = exprResult.as<double>();
    else if (exprResult.is<std::string>())
      argValue = exprResult.as<std::string>();
    else
      throw std::runtime_error(
        "In line " + getLineStr(context) + ":" + getPositionInLineStr(context) +
        "\nUnknown argument value in procedure invocation");

    procEnv->define(argName, argValue);
  }

  std::shared_ptr<Environment> currentEnv = m_environment;
  m_environment = procEnv;
  visitProcedureBlock(procContext->procedureBlock());
  m_environment = currentEnv;

  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitProcedureBlock(logoParser::ProcedureBlockContext* context)
{
  visitChildren(context);
  return antlrcpp::Any();
}

std::string
LogoVisitor::getLineStr(antlr4::ParserRuleContext* context) const
{
  return std::to_string(context->getStart()->getLine());
}

std::string
LogoVisitor::getPositionInLineStr(antlr4::ParserRuleContext* context) const
{
  return std::to_string(context->getStart()->getCharPositionInLine());
}

antlrcpp::Any
LogoVisitor::visitExpression(logoParser::ExpressionContext* context)
{
  if (context->arithmeticExpression()) {
    return visitArithmeticExpression(context->arithmeticExpression());
  } else if (context->logicalExpression()) {
    return visitLogicalExpression(context->logicalExpression());
  }
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitArithmeticExpression(
  logoParser::ArithmeticExpressionContext* context)
{
  double operandA = 0;
  double operandB = 0;
  if (context->TIMES()) {
    operandA = visitArithmeticExpression(context->arithmeticExpression()[0]);
    operandB = visitArithmeticExpression(context->arithmeticExpression()[1]);
    return antlrcpp::Any(operandA * operandB);
  } else if (context->DIV()) {
    operandA = visitArithmeticExpression(context->arithmeticExpression()[0]);
    operandB = visitArithmeticExpression(context->arithmeticExpression()[1]);
    return antlrcpp::Any(operandA / operandB);
  } else if (context->PLUS()) {
    operandA = visitArithmeticExpression(context->arithmeticExpression()[0]);
    operandB = visitArithmeticExpression(context->arithmeticExpression()[1]);
    return antlrcpp::Any(operandA + operandB);
  } else if (context->MINUS()) {
    operandA = visitArithmeticExpression(context->arithmeticExpression()[0]);
    operandB = visitArithmeticExpression(context->arithmeticExpression()[1]);
    return antlrcpp::Any(operandA - operandB);
  } else if (context->arithmeticNegation()) {
    return visitArithmeticNegation(context->arithmeticNegation());
  } else if (context->LPAREN()) {
    return visitArithmeticExpression(context->arithmeticExpression()[0]);
  } else if (context->atom()) {
    return visitAtom(context->atom());
  }
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitArithmeticNegation(
  logoParser::ArithmeticNegationContext* context)
{
  double value = visitArithmeticExpression(context->arithmeticExpression());
  return antlrcpp::Any(-value);
}

antlrcpp::Any
LogoVisitor::visitAtom(logoParser::AtomContext* context)
{
  if (context->number()) {
    return antlrcpp::Any(std::stod(context->number()->NUMBER()->getText()));
  } else if (context->deref()) {
    return visitDeref(context->deref());
  } else if (context->func()) {
    return visitFunc(context->func());
  } else if (context->stringliteral())
    return antlrcpp::Any(context->stringliteral()->STRING()->getText());
  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitLogicalExpression(
  logoParser::LogicalExpressionContext* context)
{
  bool operandA = false;
  bool operandB = false;
  if (context->AND()) {
    operandA = visitLogicalExpression(context->logicalExpression()[0]);
    operandB = visitLogicalExpression(context->logicalExpression()[1]);
    return antlrcpp::Any(operandA && operandB);
  } else if (context->OR()) {
    operandA = visitLogicalExpression(context->logicalExpression()[0]);
    operandB = visitLogicalExpression(context->logicalExpression()[1]);
    return antlrcpp::Any(operandA || operandB);
  } else if (context->comparisonExpression()) {
    return visitComparisonExpression(context->comparisonExpression());
  } else if (context->LPAREN()) {
    return visitLogicalExpression(context->logicalExpression()[0]);
  } else if (context->TRUE()) {
    return antlrcpp::Any(true);
  } else if (context->FALSE()) {
    return antlrcpp::Any(false);
  }

  return antlrcpp::Any();
}

antlrcpp::Any
LogoVisitor::visitComparisonExpression(
  logoParser::ComparisonExpressionContext* context)
{
  double operandA =
    visitArithmeticExpression(context->arithmeticExpression()[0]);
  double operandB =
    visitArithmeticExpression(context->arithmeticExpression()[1]);
  if (context->compOperator()->EQ())
    return antlrcpp::Any(std::abs(operandA - operandB) <=
                         std::numeric_limits<double>::epsilon());
  else if (context->compOperator()->GT())
    return antlrcpp::Any(operandA > operandB);
  else if (context->compOperator()->GET())
    return antlrcpp::Any(operandA >= operandB);
  else if (context->compOperator()->LT())
    return antlrcpp::Any(operandA < operandB);
  else if (context->compOperator()->GT())
    return antlrcpp::Any(operandA <= operandB);

  return antlrcpp::Any();
}
