#ifndef THROWINGERRORLISTENER_H
#define THROWINGERRORLISTENER_H

#include <BaseErrorListener.h>

class ThrowingErrorListener : public antlr4::BaseErrorListener
{
public:
  ThrowingErrorListener();

  void syntaxError(antlr4::Recognizer* recognizer,
                   antlr4::Token* offendingSymbol,
                   size_t line,
                   size_t charPositionInLine,
                   const std::string& msg,
                   std::__exception_ptr::exception_ptr e) override;
};

#endif // THROWINGERRORLISTENER_H
