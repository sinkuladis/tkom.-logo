#ifndef PARSER_H
#define PARSER_H

#include <QString>
#include <QStringList>

#include <vector>

#include "statement.h"

class Parser
{
public:
  Parser();
  void parse(const QString& plain_text);

  std::vector<Statement> getStatements() const;

private:
  std::vector<Statement> statements;
};

#endif // PARSER_H
