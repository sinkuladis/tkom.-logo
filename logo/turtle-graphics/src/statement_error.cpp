#include "statement_error.h"
#include <Token.h>

statement_error::statement_error(const std::string& msg,
                                 antlr4::ParserRuleContext* context)
  : mContext(context)
  , mLine(context->getStart()->getLine())
  , mPosition(context->getStart()->getCharPositionInLine())
  , mMsg("In line " + std::to_string(mLine) + ":" + std::to_string(mPosition) +
         "\n" + msg)
{}

statement_error::statement_error(size_t line,
                                 size_t position,
                                 const std::string& statementName)
  : mLine(line)
  , mPosition(position)
  , mStatementName(statementName)
{
  mMsg = "In line " + std::to_string(mLine) + ":" + std::to_string(mPosition) +
         "\nUsed unimplemented procedure " + mStatementName + ".";
}

const char*
statement_error::what() const noexcept
{
  return mMsg.c_str();
}
