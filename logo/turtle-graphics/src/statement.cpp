#include "statement.h"

Statement::Statement(const QString& command, const QString& value, int lineNum)
{
  bool flag;

  double m_value = value.toDouble(&flag);

  if (flag) {
    statement.second = m_value;
  } else {
    throw std::invalid_argument("Invalid command expression in line " +
                                std::to_string(lineNum) + ": \n" +
                                std::string(value.toUtf8().constData()));
  }

  auto m_command = string_command_map.find(command);

  if (m_command != string_command_map.end()) {
    statement.first = m_command->second;
    str = QString(command + " " + value);
  } else {
    throw std::invalid_argument("Uknown command in line " +
                                std::to_string(lineNum) + ": \n" +
                                std::string(command.toUtf8().constData()));
  }
}

Statement::Statement(const QString& command, double value, int lineNum)
{
  statement.second = value;
  auto m_command = string_command_map.find(command);

  if (m_command != string_command_map.end()) {
    statement.first = m_command->second;
    str = QString(command + " " + QString::number(value));
  } else {
    throw std::invalid_argument("Uknown command in line " +
                                std::to_string(lineNum) + ": \n" +
                                std::string(command.toUtf8().constData()));
  }
}

Statement::Statement()
{
  statement.first = Command::empty_;
  statement.second = 0;
}

void
Statement::execute(Turtle& turtle) const
{
  switch (statement.first) {
    case forward:
      turtle.forward(statement.second);
      break;
    case back:
      turtle.back(statement.second);
      break;
    case left:
      turtle.left(statement.second);
      break;
    case right:
      turtle.right(statement.second);
      break;
    case clearscreen:
      turtle.clearscreen();
      break;
    case home:
    case setpos:
      throw std::runtime_error("Unimplemented statements");
    case print:
      turtle.print(statement.second);
    case empty_:
      break;
  }
}

std::pair<Command, double>
Statement::getStatement() const
{
  return statement;
}

void
Statement::setStatement(Command cmd, double value)
{
  statement = std::pair<Command, double>(cmd, value);
}
