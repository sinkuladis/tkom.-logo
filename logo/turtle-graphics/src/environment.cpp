#include "environment.h"

Environment::Environment()
{
  m_enclosing.reset();
}

Environment::Environment(std::shared_ptr<Environment> enclosing)
{
  m_enclosing = enclosing;
}

object
Environment::get(const std::string name) const
{
  if (m_values.find(name) != m_values.end()) {
    auto valueIter = m_values.find(name);
    return valueIter->second;
  }
  if (m_enclosing) {
    return m_enclosing->get(name);
  }

  throw std::runtime_error("Undefined variable '" + name + "'.");
}

void
Environment::assign(const std::string name, const object newValue)
{
  if (m_values.find(name) != m_values.end()) {
    auto valueIter = m_values.find(name);
    valueIter->second = newValue;
    return;
  }
  if (m_enclosing) {
    m_enclosing->assign(name, newValue);
    return;
  }

  throw std::runtime_error("Undefined variable '" + name + "'.");
}

void
Environment::define(const std::string name, const object value)
{
  m_values.insert_or_assign(name, value);
}
