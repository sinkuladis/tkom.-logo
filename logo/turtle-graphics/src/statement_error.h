#ifndef STATEMENT_ERROR_H
#define STATEMENT_ERROR_H

#include <ParserRuleContext.h>
#include <exception>
#include <string>

// TODO: passing rule context only should be enough
class statement_error : std::exception
{
public:
  statement_error(const std::string& msg, antlr4::ParserRuleContext* context);

  statement_error(size_t line,
                  size_t position,
                  const std::string& statementName);

  size_t line() const;
  size_t position() const;

private:
  antlr4::ParserRuleContext* mContext;
  size_t mLine;
  size_t mPosition;
  std::string mStatementName;
  std::string mMsg;

  const char* what() const noexcept override;
};

#endif // STATEMENT_ERROR_H
