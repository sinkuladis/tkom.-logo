#include "interpreter.h"
#include "logovisitor.h"

Interpreter::Interpreter(shared_ptr<Turtle> turtle) { m_turtle = turtle; }

void Interpreter::tokenize(const string &text) const {

  string line;
  istringstream modelicaFile(text);
  antlr4::ANTLRInputStream input(modelicaFile);
  logoLexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);

  tokens.fill();

  for (auto token : tokens.getTokens()) {
    std::cout << token->toString() << std::endl;
  }
}

void Interpreter::ast(const string &text) const {
  string line;
  istringstream modelicaFile(text);
  antlr4::ANTLRInputStream input(modelicaFile);
  logoLexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);

  tokens.fill();

  logoParser parser(&tokens);
  antlr4::tree::ParseTree *tree = parser.prog();

  std::cout << tree->toStringTree(&parser) << std::endl;
}

void Interpreter::run(const string &text) const {
  string line;
  istringstream modelicaFile(text);
  antlr4::ANTLRInputStream input(modelicaFile);
  logoLexer lexer(&input);
  antlr4::CommonTokenStream tokens(&lexer);
  logoParser parser(&tokens);

  logoParser::ProgContext *tree = parser.prog();

  LogoVisitor visitor(m_turtle);
  visitor.visitProg(tree);
}
