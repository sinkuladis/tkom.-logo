#include "parser.h"

Parser::Parser() {}

void
Parser::parse(const QString& plainText)
{
  std::vector<Statement> new_statements;
  QStringList strList = plainText.split("\n");
  int lineNum = 0;
  for (const auto& line : strList) {
    QStringList parsed_line = line.split(" ");
    ++lineNum;
    if (parsed_line.size() == 2) {
      Statement stmt(parsed_line[0], parsed_line[1], lineNum);
      new_statements.push_back(stmt);
    } else if (line == "") {
      continue;
    } else {
      throw std::invalid_argument("Invalid statement at line " +
                                  std::to_string(lineNum) + ": \n" +
                                  std::string(line.toUtf8().constData()));
    }
  }
  statements = new_statements;
}

std::vector<Statement>
Parser::getStatements() const
{
  return statements;
}
