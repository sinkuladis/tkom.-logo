#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  populateScene();
}

MainWindow::~MainWindow() {}

void
MainWindow::on_clear_clicked()
{
  //  ui->textEdit->clear();
  turtle->clearscreen();
  ui->graphicsView->clearFocus();
  turtle = new Turtle();
  scene->addItem(turtle);
}

void
MainWindow::on_run_clicked()
{
  QString text = ui->textEdit->toPlainText();
  try {
    LogoVisitor visitor(turtle);
    ui->history->appendPlainText(QString(text + "\n----------"));
    visitor.run(text.toUtf8().constData());
  } catch (std::exception& e) {
    informationMessage(e.what());
    //    editor.highlightWord();
  }
}

void
MainWindow::populateScene()
{
  scene = std::make_shared<QGraphicsScene>(ui->graphicsView);
  ui->graphicsView->setScene(scene.get());

  turtle = new Turtle();
  scene->addItem(turtle);
}

void
MainWindow::errorMessage(const std::string& error) const
{
  errorMessageDialog->showMessage(error.data());
}

void
MainWindow::informationMessage(const std::string& err) const
{
  QMessageBox::information(
    ui->centralWidget, tr("Statement error"), QDialog::tr(err.data()));
}

QString
MainWindow::getOpenFileName()
{
  QString selectedFilter;
  QString fileName =
    QFileDialog::getOpenFileName(this,
                                 tr("Open file"),
                                 nullptr,
                                 tr("All Files (*);;Text Files (*.txt)"),
                                 &selectedFilter);
  if (fileName.isEmpty())
    return "";
  return fileName;
}

QString
MainWindow::getSaveFileName()
{
  QString selectedFilter;
  QString fileName =
    QFileDialog::getSaveFileName(this,
                                 tr("Save file"),
                                 nullptr,
                                 tr("All Files (*);;Text Files (*.txt)"),
                                 &selectedFilter);
  if (fileName.isEmpty())
    return "";
  return fileName;
}

void
MainWindow::on_actionOpen_triggered()
{
  QString fileName = getOpenFileName();
  std::ifstream openFile(fileName.toUtf8().constData());
  ui->textEdit->clear();
  std::string line;
  while (getline(openFile, line)) {
    ui->textEdit->insertPlainText(line.data());
    ui->textEdit->insertPlainText("\n");
  }
  openFile.close();
}

void
MainWindow::on_actionSave_triggered()
{
  QString fileName = getSaveFileName();
  std::ofstream openFile(fileName.toUtf8().constData());
  openFile << ui->textEdit->toPlainText().toUtf8().constData();
  openFile.close();
}
