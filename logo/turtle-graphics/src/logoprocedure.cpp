#include "logoprocedure.h"

LogoProcedure::LogoProcedure(logoParser::ProcedureDeclarationContext* context)
  : m_context(context)
  , m_argumentsNames(context->parameterDeclarations())
{}

size_t
LogoProcedure::arity()
{
  return m_context->parameterDeclarations().size();
}

void
LogoProcedure::call(LogoVisitor visitor, std::vector<object> arguments)
{
  std::shared_ptr<Environment> environment = visitor.globals();

  for (size_t i = 0; i < arity(); ++i) {
    environment->define(m_argumentsNames[i]->name()->STRING()->getText(),
                        arguments[i]);
  }

  visitor.visitProcedureBlock(m_context->procedureBlock());
}
