grammar logo;

// used for formatting 
// $antlr-format on
// signExpression : (('+' | '-'))* (number | deref | func) ;// $antlr-format false
// $antlr-format columnLimit 150
// $antlr-format allowShortBlocksOnASingleLine true, indentWidth 8

prog
   : (line? EOL) + line*
   ; 
  
line
   : comment
   | procedureDeclaration
   | cmd + comment?
   ;
 
cmd
   : statement
   | control
   | make
   | procedureInvocation
   | print comment?
   // | stop
   ; 

control
   : repeat
   | ife
   | fore
   ;

statement
   : fd
   | bk
   | rt
   | lt
   | cs
   | pu
   | pd
   | ht
   | st
   | home
   | label
   | setxy
   ;
 
procedureInvocation
   : name expression*
   ;
  
procedureDeclaration
   : 'to' name parameterDeclarations* EOL? procedureBlock 'end'
   ;

procedureBlock
   :(line? EOL) +
   ;

parameterDeclarations
   : ':' name 
   ;

func
   : random
   | sin
   | cos
   ;

repeat
   : 'repeat' number block
   ;

block
   : '[' cmd + ']'
   ;

ife
   : 'if' (expression | '[' expression']') block
   ;

comparisonOperator
   : EQ
   | GT
   | LT
   | GET
   | LET
   ;

make
   : 'make' stringliteral value
   ;
 
print
   : 'print' value
   ;

name
   : STRING
   ;

value
   : expression
   | deref
   ;

expression
   : logicalExpression | arithmeticExpression
   ;

arithmeticExpression
   : arithmeticExpression TIMES arithmeticExpression
   | arithmeticExpression DIV arithmeticExpression
   | arithmeticExpression PLUS arithmeticExpression
   | arithmeticExpression MINUS arithmeticExpression
   | arithmeticNegation
   | LPAREN arithmeticExpression RPAREN
   | atom
   ;
 
arithmeticNegation
   : MINUS arithmeticExpression
   ;

logicalExpression
   : AND logicalExpression logicalExpression 
   | OR logicalExpression logicalExpression
   | comparisonExpression
   | LPAREN logicalExpression RPAREN
   | TRUE
   | FALSE
   ;

comparisonExpression
   : arithmeticExpression compOperator arithmeticExpression
   ;

compOperator 
   : GT
   | GET
   | LT
   | LET
   | EQ
   ;

atom
   : number
   | deref
   | func 
   | stringliteral
   ;

deref
   : ':' name
   ;

fd
   : ('fd' | 'forward') expression
   ;

bk
   : ('bk' | 'backward') expression
   ;

rt
   : ('rt' | 'right') expression
   ;

lt
   : ('lt' | 'left') expression
   ;

cs
   : 'cs'
   | 'clearscreen'
   ;

pu
   : 'pu'
   | 'penup'
   ;

pd
   : 'pd'
   | 'pendown'
   ;

ht
   : 'ht'
   | 'hideturtle'
   ;

st
   : 'st'
   | 'showturtle'
   ;

home
   : 'home'
   ;

stop
   : 'stop'
   ;

label
   : 'label'
   ;

setxy
   : 'setxy' expression expression
   ;

random
   : 'random' expression
   ;

sin
   : 'sin' arithmeticExpression
   ;

cos
   : 'cos' arithmeticExpression
   ;

fore
   : 'for' '[' name expression expression expression? ']' block
   ;

number
   : NUMBER
   ;

comment
   : COMMENT
   ;

stringliteral
   : '"' STRING
   ;

fragment SIGN
   : (PLUS | MINUS)
   ;

// fragment STRINGLITERAL
//    : '"' STRING
//    ;

LPAREN: '(';
RPAREN: ')';

PLUS: '+';
MINUS: '-';
TIMES: '*';
DIV: '/';

GT: '>';
GET: '>=';
LT: '<';
LET: '<=';
EQ: '=';

AND : 'and' ;
OR  : 'or' ;
NOT : 'not';

TRUE  : 'true' ;
FALSE : 'false' ;

STRING
   : [a-zA-Z] [a-zA-Z0-9_]*
   ;

NUMBER
   : ('0' .. '9') + ('.' ('0' .. '9') +)?
   ;
   
COMMENT
   : ';' ~ [\r\n]*
   ;

EOL
   : '\r'? '\n'
   ;

WS
   : [ \t\r\n] -> skip
   ;