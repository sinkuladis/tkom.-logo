#ifndef TST_VISITORCASE_H
#define TST_VISITORCASE_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include <src/logovisitor.h>

using namespace testing;
using namespace antlrcpp;

TEST(Visitor, Statement)
{
  EXPECT_EQ(1, 1);
  ASSERT_THAT(0, Eq(0));
}

#endif // TST_VISITORCASE_H
