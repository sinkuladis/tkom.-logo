#ifndef TST_ENVIRONMENT_H
#define TST_ENVIRONMENT_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "environment.h"

using namespace antlrcpp;

TEST(Environment, Define)
{
  Environment env;
  env.define("test_double", 2);
  EXPECT_EQ(std::get<double>(env.get("test_double")), 2);
  //  EXPECT_STREQ(std::get<std::string>(env.get("test_string")), "test");
}

TEST(Environment, Assign)
{
  Environment env;
  EXPECT_THROW(env.assign("test", "test"), std::runtime_error);
}

#endif // TST_ENVIRONMENT_H
