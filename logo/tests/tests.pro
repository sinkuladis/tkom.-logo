include(gtest_dependency.pri)

QT += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
CONFIG += console c++17 no_keywords
CONFIG -= app_bundle
CONFIG += thread
#CONFIG -= qt

#QMAKE_CXXFLAGS += --coverage
#QMAKE_LFLAGS += --coverage

HEADERS += \
        ../turtle-graphics/generated/logoBaseListener.h \
        ../turtle-graphics/generated/logoBaseVisitor.h \
        ../turtle-graphics/generated/logoLexer.h \
        ../turtle-graphics/generated/logoListener.h \
        ../turtle-graphics/generated/logoParser.h \
        ../turtle-graphics/generated/logoVisitor.h \
        ../turtle-graphics/src/codeeditor.h \
        ../turtle-graphics/src/environment.h \
        ../turtle-graphics/src/interpreter.h \
        ../turtle-graphics/src/logoprocedure.h \
        ../turtle-graphics/src/logovisitor.h \
        ../turtle-graphics/src/mainwindow.h \
        ../turtle-graphics/src/parser.h \
        ../turtle-graphics/src/statement.h \
        ../turtle-graphics/src/statement_error.h \
        ../turtle-graphics/src/throwingerrorlistener.h \
        ../turtle-graphics/src/turtle.h \
        tst_environmentcase.h \
        tst_statementcase.h \
        tst_visitorcase.h

SOURCES += \
        main.cpp \
    ../turtle-graphics/generated/logoBaseListener.cpp \
    ../turtle-graphics/generated/logoBaseVisitor.cpp \
    ../turtle-graphics/generated/logoLexer.cpp \
    ../turtle-graphics/generated/logoListener.cpp \
    ../turtle-graphics/generated/logoParser.cpp \
    ../turtle-graphics/generated/logoVisitor.cpp \
    ../turtle-graphics/src/codeeditor.cpp \
    ../turtle-graphics/src/environment.cpp \
    ../turtle-graphics/src/logoprocedure.cpp \
    ../turtle-graphics/src/logovisitor.cpp \
    ../turtle-graphics/src/mainwindow.cpp \
    ../turtle-graphics/src/parser.cpp \
    ../turtle-graphics/src/statement.cpp \
    ../turtle-graphics/src/throwingerrorlistener.cpp \
    ../turtle-graphics/src/turtle.cpp \
    ../turtle-graphics/src/statement_error.cpp

FORMS += \
    ../turtle-graphics/forms/mainwindow.ui

DESTDIR = $$PWD/../build/tests

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/release/ -lantlr4-runtime
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/debug/ -lantlr4-runtime
else:unix: LIBS += -L$$PWD/../3rdparty/antlr4-runtime/lib/ -lantlr4-runtime

INCLUDEPATH += \
    $$PWD/../3rdparty/antlr4-runtime/include \
    $$PWD/../turtle-graphics/src \
    $$PWD/../turtle-graphics \
    $$PWD/../3rdparty
DEPENDPATH += \
    $$PWD/../3rdparty/antlr4-runtime/include \
    $$PWD/../turtle-graphics/src \
    $$PWD/../turtle-graphics \
    $$PWD/../3rdparty
