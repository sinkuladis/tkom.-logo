#include "tst_environmentcase.h"
#include "tst_statementcase.h"
#include "tst_visitorcase.h"

#include <gtest/gtest.h>

// TODO: Otpimize project dependencies. Refactor .pro file
int
main(int argc, char* argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
