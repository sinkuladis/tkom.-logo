#ifndef TST_STATEMENTCASE_H
#define TST_STATEMENTCASE_H

#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "statement.h"

TEST(Statement, GetStatement)
{
  Statement stmt("forward", 20);
  std::pair<Command, double> mStmt = stmt.getStatement();
  EXPECT_EQ(mStmt.first, Command::forward);
  EXPECT_EQ(mStmt.second, 20);
}

TEST(Statement, SetStatement)
{
  Statement stmt("forward", 20);
  stmt.setStatement(Command::back, -20);
  std::pair<Command, double> mStmt = stmt.getStatement();
  EXPECT_EQ(mStmt.first, Command::back);
  EXPECT_EQ(mStmt.second, -20);
}

// TEST(Statement, Execute)
//{
//  Turtle turtle;
//  QGraphicsView gView;
//  gView.scene()->addItem(&turtle);
//  gView.scene()->items();

//  Statement stmt("forward", 20);
//  stmt.execute(turtle);

//  EXPECT_EQ(turtle.getX(), 20);
//}

#endif // TST_STATEMENTCASE_H
