/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "src/codeeditor.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QSplitter *splitter_2;
    QSplitter *splitter;
    QGraphicsView *graphicsView;
    QTabWidget *tabWidget;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *history;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QTextEdit *textEdit_2;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QPushButton *run;
    QPushButton *clear;
    CodeEditor *textEdit;
    QMenuBar *menuBar;
    QMenu *menuFile;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(820, 808);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        QIcon icon;
        QString iconThemeName = QString::fromUtf8("document-open");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon = QIcon::fromTheme(iconThemeName);
        } else {
            icon.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionOpen->setIcon(icon);
        actionSave = new QAction(MainWindow);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        QIcon icon1;
        iconThemeName = QString::fromUtf8("document-save");
        if (QIcon::hasThemeIcon(iconThemeName)) {
            icon1 = QIcon::fromTheme(iconThemeName);
        } else {
            icon1.addFile(QString::fromUtf8("."), QSize(), QIcon::Normal, QIcon::Off);
        }
        actionSave->setIcon(icon1);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setBaseSize(QSize(0, 0));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        splitter_2 = new QSplitter(centralWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        splitter = new QSplitter(splitter_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        graphicsView = new QGraphicsView(splitter);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(4);
        sizePolicy2.setVerticalStretch(1);
        sizePolicy2.setHeightForWidth(graphicsView->sizePolicy().hasHeightForWidth());
        graphicsView->setSizePolicy(sizePolicy2);
        graphicsView->setMinimumSize(QSize(500, 570));
        graphicsView->setSizeIncrement(QSize(0, 0));
        graphicsView->setInteractive(true);
        graphicsView->setRenderHints(QPainter::Antialiasing|QPainter::HighQualityAntialiasing|QPainter::SmoothPixmapTransform|QPainter::TextAntialiasing);
        splitter->addWidget(graphicsView);
        tabWidget = new QTabWidget(splitter);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(1);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy3);
        tabWidget->setMinimumSize(QSize(250, 0));
        widget = new QWidget();
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        history = new QPlainTextEdit(widget);
        history->setObjectName(QString::fromUtf8("history"));
        history->setTextInteractionFlags(Qt::TextSelectableByKeyboard|Qt::TextSelectableByMouse);

        verticalLayout->addWidget(history);

        tabWidget->addTab(widget, QString());
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        textEdit_2 = new QTextEdit(tab);
        textEdit_2->setObjectName(QString::fromUtf8("textEdit_2"));
        textEdit_2->setReadOnly(true);

        verticalLayout_2->addWidget(textEdit_2);

        tabWidget->addTab(tab, QString());
        splitter->addWidget(tabWidget);
        splitter_2->addWidget(splitter);
        layoutWidget = new QWidget(splitter_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        run = new QPushButton(layoutWidget);
        run->setObjectName(QString::fromUtf8("run"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(1);
        sizePolicy4.setVerticalStretch(5);
        sizePolicy4.setHeightForWidth(run->sizePolicy().hasHeightForWidth());
        run->setSizePolicy(sizePolicy4);
        run->setMinimumSize(QSize(127, 100));
        run->setBaseSize(QSize(0, 0));

        gridLayout->addWidget(run, 0, 1, 1, 1);

        clear = new QPushButton(layoutWidget);
        clear->setObjectName(QString::fromUtf8("clear"));
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy5.setHorizontalStretch(1);
        sizePolicy5.setVerticalStretch(1);
        sizePolicy5.setHeightForWidth(clear->sizePolicy().hasHeightForWidth());
        clear->setSizePolicy(sizePolicy5);
        clear->setMinimumSize(QSize(0, 0));
        clear->setBaseSize(QSize(0, 0));

        gridLayout->addWidget(clear, 1, 1, 1, 1);

        textEdit = new CodeEditor(layoutWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy6.setHorizontalStretch(4);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(textEdit->sizePolicy().hasHeightForWidth());
        textEdit->setSizePolicy(sizePolicy6);

        gridLayout->addWidget(textEdit, 0, 0, 2, 1);

        splitter_2->addWidget(layoutWidget);

        gridLayout_2->addWidget(splitter_2, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 820, 22));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);
        menuFile->addAction(actionSave);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Turtle graphics", nullptr));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", nullptr));
#ifndef QT_NO_SHORTCUT
        actionOpen->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionSave->setText(QApplication::translate("MainWindow", "Save", nullptr));
#ifndef QT_NO_SHORTCUT
        actionSave->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        tabWidget->setTabText(tabWidget->indexOf(widget), QApplication::translate("MainWindow", "History", nullptr));
        textEdit_2->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><title>Logo Language Guide</title><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\" bgcolor=\"transparent\">\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"types\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">D</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">ata Types and Syntax </span></h3>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">&quot;</span><span style=\" font-family:'monospa"
                        "ce'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">word</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Word. (Quoted words are terminated by [](){} or whitespace, \\ to escape.) </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print &quot;hello</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print 12.34</span><span style=\" font-family:'sans-serif'; color:#000000;"
                        "\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">:</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">variable</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Input definition/variable reference </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">show :name</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-b"
                        "ottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">( </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expression</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> )</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Parenthesis can be used to group expressions </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">show ( 1 + 2 ) * 3</span><span style=\" font-family:'sans-serif'; color:#"
                        "000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">procedure</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">input</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ...</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Call procedure with default number of inputs </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:8px; margin-left:30px; margin-right:0px; -"
                        "qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print &quot;hello</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec3\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">2</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">. Communication </span></h3>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">2.1 Transmitters </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; colo"
                        "r:#000000;\">print </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">thing</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print &quot;hello</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">type &quot;hel  type &quot;lo</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#00"
                        "0000;\">(</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">show </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">thing1</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">thing2</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ...)</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Like </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">print</span><span style=\" font-family:'sans-serif'; font-size:10pt; color"
                        ":#000000;\"> but with square brackets around list inputs. </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print &quot;hello</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec4\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">3</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">. Arithmetic </span></h3>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">3.1 Numeric Operations </span></h4>\n"
"<p style=\" margin-top:0px; margin-bo"
                        "ttom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Inputs are numbers or numeric expressions, output is a number.</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> + </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:"
                        "10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> - </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> * </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-"
                        "indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> / </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">- </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style="
                        "\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Unary minus sign must begin a top-level expression, follow an infix operator, or have a leading space and no trailing space. </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">sin </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">cos </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-s"
                        "ize:10pt; color:#000000;\"> </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">3.2 Numeric Predicates </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> &lt; </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:"
                        "10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> &gt; </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> &lt;= </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt"
                        "-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> &gt;= </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Less than, greater than, less than or equal to, greater than or equal to, respectively. Inputs are numbers or numeric expressions, output is 1 (true) or 0 (false). </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:mediu"
                        "m; font-weight:600; color:#000000;\">3.3 Random Numbers </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">random </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs a random number from 0 through one less than </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">, or from start to end inclusive. </span></p>\n"
"<p style=\" margin-top:2px; marg"
                        "in-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">print random 10</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec5\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">4</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">. Logical Operations </span></h3>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">true</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0"
                        "px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs 1 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">false</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs 0 </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">and </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><spa"
                        "n style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">or </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin"
                        "-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">not </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Logical &quot;and&quot;, &quot;or&quot;, &quot;exclusive-or&quot;, and &quot;not&quot;, respectively. Inputs are numbers or numeric expressions, output is 1 (true) or 0 (false). </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec6\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">5</span><span style=\" font-family:'s"
                        "ans-serif'; font-size:large; font-weight:600; color:#000000;\">. Graphics </span></h3>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">5.1 Turtle Motion </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">forward </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">fd </span><span style=\" font-family"
                        ":'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Move turtle forward </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> pixels </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">fd 100</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style"
                        "=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">back </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">bk </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Move turtle backward </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style"
                        ":italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> pixels </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">bk 100</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">left </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10"
                        "pt; font-weight:600; color:#000000;\">lt </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Rotate </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> degrees counterclockwise </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">lt 90</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-"
                        "left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">right </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">rt </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Rotate </span><s"
                        "pan style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> degrees clockwise </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">rt 90</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">setpos [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:ita"
                        "lic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">setxy </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'"
                        "monospace'; font-size:10pt; font-weight:600; color:#000000;\">setx </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">sety </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Move turtle to the specified location </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-le"
                        "ft:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">setpos [ 100 -100 ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">setxy -100 100</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">home</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; co"
                        "lor:#000000;\">Moves the turtle to center, pointing upwards </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">5.2 Turtle Motion Queries </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">pos</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">xcor</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; ma"
                        "rgin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">ycor</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs the current turtle position as [ x y ], x or y respectively </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">show pos</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">he"
                        "ading</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs the current turtle heading </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">show heading</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">5.3 Turtle and Window Control </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><sp"
                        "an style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">showturtle</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">st</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Show the turtle </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">hideturtle</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:"
                        "#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">ht</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Hide the turtle </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">clearscreen</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" fo"
                        "nt-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">cs</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Same as clean and home together </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">5.4 Pen and Background Control </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">pendown</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; m"
                        "argin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">pd</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Turtle resumes leaving a trail </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">penup</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weig"
                        "ht:600; color:#000000;\">pu</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Turtle stops leaving a trail </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec7\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">6</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">. Workspace Management </span></h3>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">6.1 Procedure Definition </span></h4>\n"
"<p style=\" margin-top:0px;"
                        " margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">to </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">procname</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">inputs ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> end</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px"
                        "; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Define a new named procedure. Inputs can be: </span></p>\n"
"<ul style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;\"><li style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\" style=\" margin-top:12px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Required: <span style=\" font-family:'monospace'; font-weight:600;\">:a :b</span> </li>\n"
"<li style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\" style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Optional (with default values): <span style=\" font-family:'monospace'; font-weight:600;\">[:c 5] [:d 7]</span> </li>\n"
"<li style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\" style=\" margin-top:0px; margin-bottom:0px; margin-lef"
                        "t:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Rest (remaining inputs as a list): <span style=\" font-family:'monospace'; font-weight:600;\">[:r]</span> </li>\n"
"<li style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\" style=\" margin-top:0px; margin-bottom:12px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Default number of inputs: <span style=\" font-family:'monospace'; font-weight:600;\">3</span> </li></ul>\n"
"<p style=\" margin-top:2px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">to star :n  repeat 5 [ fd :n rt 144 ]  end</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">6.2 Varia"
                        "ble Definition </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">make </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">varname</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Update a variable or define a new global variable. The variable name must be quoted </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:"
                        "8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">make &quot;myvar 5</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<h3 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><a name=\"sec8\"></a><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">7</span><span style=\" font-family:'sans-serif'; font-size:large; font-weight:600; color:#000000;\">. Control Structures </span></h3>\n"
"<h4 style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:medium; font-weight:600; color:#000000;\">7.1 Control </span></h4>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monosp"
                        "ace'; font-size:10pt; font-weight:600; color:#000000;\">repeat </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Repeat statements </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> times </span></"
                        "p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">repeat 4 [ fd 100 rt 90 ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">repcount</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">#</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-bl"
                        "ock-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Outputs the current iteration number of the current </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">repeat</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> or </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">forever</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">repeat 10 [ show repcount ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:"
                        "10pt; color:#808080;\">repeat 10 [ show # ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">if </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" fon"
                        "t-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">if [</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">] [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Execute statements if the expression is non-zero </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" fon"
                        "t-family:'monospace'; font-size:10pt; color:#808080;\">if 2 &gt; 1 [ show &quot;yep ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">ifelse </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ] [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:1"
                        "0pt; font-weight:600; color:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">ifelse [</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">expr</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">] [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> ] [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; colo"
                        "r:#000000;\"> ]</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Execute first set of statements if the expression is non-zero, otherwise execute the second set </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">ifelse 1 &gt; 2 [ print &quot;yep ] [ print &quot;nope ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">stop</span><span style=\" font-family:'sans-serif'; color:#0"
                        "00000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">End the running procedure with no output value.</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">for </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">controllist</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\"> [ </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; font-style:italic; color:#000000;\">statements ...</span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">]</sp"
                        "an><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">Typical </span><span style=\" font-family:'monospace'; font-size:10pt; font-weight:600; color:#000000;\">for</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> loop. The </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">controllist</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> specifies three or four members: the local </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">varname</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\">, </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">start</sp"
                        "an><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> value, </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">limit</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> value, and optional </span><span style=\" font-family:'sans-serif'; font-size:10pt; font-style:italic; color:#000000;\">step</span><span style=\" font-family:'sans-serif'; font-size:10pt; color:#000000;\"> size. </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:2px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">for [ a 1 10 ] [ print :a ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p>\n"
"<p style=\" margin-top:2px; margin-bottom:8px; margin-left:30px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'monospace'; font-size:10pt; color:#808080;\">for [ a 0 20 2 ] [ print :"
                        "a ]</span><span style=\" font-family:'sans-serif'; color:#000000;\"> </span></p></body></html>", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Reference", nullptr));
        run->setText(QApplication::translate("MainWindow", "Run", nullptr));
        clear->setText(QApplication::translate("MainWindow", "Clear", nullptr));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
